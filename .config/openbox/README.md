# Openbox Config
![Screenshot of Openbox](https://gitlab.com/fawzadin/dotfiles/raw/master/screenshot/openbox.png)

Openbox is a lightweight and highly-configurable floating window manager.
This config is originally made just for leisure but I can see some use cases of using it like:
+ For application that heavily relies on floating window environment like video games.
+ Testing your GUI application.
+ When your peer cannot grasp the idea of tilling window manager as you share your screen on an online meeting.

I don't know what I should put here on this readme so here are the features:
+ My own tint2 config.
+ Move window with mod+LMB.
+ jgmenu for start menu (mod+a).
+ Three workspaces are enough for openbox since who uses them in a floating wm? 
+ Nothing stands out really.

