" ctrl+6 to return to previous file

" Disable GUI Tabline
if exists(':GuiTabline')
    GuiTabline 0
endif

" Disable GUI Popupmenu
if exists(':GuiPopupmenu')
    GuiPopupmenu 0
endif

" Set the font in case GUI Neovim doesn't recognize it in init.vim
"GuiFont! FuraCode\ NF:h12
