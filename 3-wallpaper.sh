#!/bin/sh
# This script can be executed from "2-install.sh"

mkdir -p $HOME/Pictures/Wallpaper/Vaporwave
mkdir -p $HOME/Pictures/Wallpaper/VG

# Vaporwave/Synthwave Wallpapers
cd $HOME/Pictures/Wallpaper/Vaporwave

curl -o Betrayal.png https://images8.alphacoders.com/754/754593.png

curl -o Wireframe\ Sun.jpg https://getwallpapers.com/wallpaper/full/6/5/f/1390095-most-popular-vaporwave-wallpapers-2560x1440-free-download.jpg

curl -o Red\ Sun.jpg https://images3.alphacoders.com/112/1120921.jpg

curl -o Fuji.jpg https://images2.alphacoders.com/104/1042787.jpg

curl -o Dark\ Sun.jpg https://images.alphacoders.com/925/925465.png


# Video Game Wallpapers
cd $HOME/Pictures/Wallpaper/VG

# These PMD Wallpapers are made by me.
# Feel free to use and share this.
# I miss my PMD OCs, Tekka and Pyra.
# Link https://totallyauselesstrashcan.tumblr.com/post/646810977641299968/through-the-sea-of-time-posting-this-image-to

curl -o Sea\ of\ Time\ Day.png https://64.media.tumblr.com/860c80a53f2cd33b39b089fe91238ffa/8f64bac28756a10e-74/s2048x3072/22bbf6e1243648f78ad712c4b1a0c55eea18f84d.png

curl -o Sea\ of\ Time\ Night.png https://64.media.tumblr.com/bcf4de099b4bd39e4464bb2181e9dd0c/8f64bac28756a10e-dd/s2048x3072/b359d411177c96f473cf43a330e4593b25e927fd.png

# Monster Hunter 15th Anniversary art by Capcom
curl -o MH15th.jpg https://www.monsterhunter.com/15th/images/top/mainart.jpg

# Put every image to /usr/share/backgrounds/custom
# This is for lightdm gtk greeter that cannot read any image from user's home folder.

echo "Copying all wallpaper images to /usr/share/backgrounds/custom/ for lightdm."
echo "Please enter your sudo password."

sudo mkdir -p /usr/share/backgrounds/custom/
sudo cp -r $HOME/Pictures/Wallpaper/* /usr/share/backgrounds/custom/

echo "Wallpapers have been downloaded!"
echo "See WALLPAPER.md to check out more wallpapers to (manually) download."
