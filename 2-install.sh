#!/bin/sh
# This script will install all necessary configuration and, optionally, any combination of dwm, openbox, or wallpaper

# Will quit the script if running in root
[ "$(id -u)" = "0" ] && \
    echo "> WARNING: This script cannot be run as root as it would cause a big mess." && \
    echo "> Please rerun this script as regular user (without sudo)." && \
    exit 1

# Set some variable.
set -o nounset # error when referencing undefined variable.
set -o errexit # exit when a command fails.

# Setting up our current script and source path.
crt="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
crtsrc=$crt/src
mkdir -p $crtsrc

# Variable for various functions
installedSucklessDeps=0
autocolor=null
setterm=null

confirm() {
    printf "$* [y/N]? "
    read -r REPLY
    [ "$REPLY" = "y" ] || [ "$REPLY" = "Y" ]
}


#=====================#
#||   Main Install  ||#
#=====================#

# Pick your desired colorscheme. 
# If you don't run this, Matcha-dark-alice/red will be set. 
pickColor() {
    printf "> Choose colorscheme: Red, Blue, or Nord (R/b/n)? "
    read -r REPLY
    case $REPLY in
        b|B ) echo "> Picked Blue colorscheme."
            autocolor=blue
             ;;
        n|N ) echo "> Picked Nord colorscheme."
            autocolor=nord
            ;;
        * ) echo "> Picked Red colorscheme."
            autocolor=red
            ;;
    esac
}

# copy config files to current user's home directory and other configurations.
initial() {
    echo "> Copying configs. Enter sudo password if needed."
    cd $crt
    cp -r $HOME/.config $HOME/.config.bak
    cp -rf .config/ .local/ $HOME/
    cp other/.zshenv $HOME/.zshenv 
 
    sudo cp other/lightdm-gtk-greeter.conf /etc/lightdm/lightdm-gtk-greeter.conf
    sudo cp other/90-backlight.rules /etc/udev/rules.d/90-backlight.rules
    
    # Set if you want to use st or Alacritty
    printf "> Which Terminal do you want to use? (a)lacritty or (s)t [a/S]? "
    read -r REPLY
    case "$REPLY" in
        a|A ) echo "> Using Alacritty as default terminal." 
            sed -i -e 's/TERMINAL=\"st\"/TERMINAL=\"alacritty\"/g' $HOME/.zshenv 
            sed -i -e 's/st\ -d/alacritty\ --working-directory/g' $HOME/.config/Thunar/uca.xml
            setterm=1 # Variable for terminal setting. 1: Alacritty.
            ;;
        * ) echo "> Using st as default terminal."
            setterm=0 # 0: st.
            ;;
    esac
    
    # Add user to video group for controlling backlight
    echo "> Adding crt user to video group..."
    # How could an existing group a total error?
    grep -q "video" /etc/group || sudo groupadd video
    sudo usermod -aG video $USER
    
    # Fixing flameshot config
    echo "> Adding Screenshots directory for flameshot."
    mkdir -p $HOME/Pictures/Screenshots
    sed -i -e "s|HOMEDIR|$HOME|" $HOME/.config/flameshot/flameshot.ini
    sed -i -e "s|HOMEDIR|$HOME|" $crt/other/flameshot.conf

    if [ "$autocolor" = "blue" ]; then
        sed -i -e 's|uiColor=#ef2929|uiColor=#3465a4|g' $HOME/.config/flameshot/flameshot.ini
        sed -i -e 's|uiColor=#ef2929|uiColor=#3465a4|g' $crt/other/flameshot.conf
        sed -i -e 's|colors: \*alice|colors: \*azure|' $HOME/.config/alacritty/alacritty.yml
        sed -i -e 's|Matcha-dark-aliz|Matcha-dark-azul|g' $HOME/.config/gtk-2.0/gtkrc
        sed -i -e 's|Matcha-dark-aliz|Matcha-dark-azul|g' $HOME/.config/gtk-3.0/settings.ini
        sudo sed -i -e 's|Matcha-dark-aliz|Matcha-dark-azul|g' /etc/lightdm/lightdm-gtk-greeter.conf
    elif [ "$autocolor" = "nord" ]; then
        sed -i -e 's|uiColor=#ef2929|uiColor=#68759f|g' $HOME/.config/flameshot/flameshot.ini
        sed -i -e 's|uiColor=#ef2929|uiColor=#68759f|g' $crt/other/flameshot.conf
        sed -i -e 's|colors: \*alice|colors: \*nord|' $HOME/.config/alacritty/alacritty.yml
        sed -i -e 's|Matcha-dark-aliz|Nordic-darker-standard-buttons|g' $HOME/.config/gtk-2.0/gtkrc
        sed -i -e 's|Matcha-dark-aliz|Nordic-darker-standard-buttons|g' $HOME/.config/gtk-3.0/settings.ini
        sudo sed -i -e 's|Matcha-dark-aliz|Nordic-darker-standard-buttons|g' /etc/lightdm/lightdm-gtk-greeter.conf
    elif [ "$autocolor" = "red" ]; then
        :
    else
        echo "> You did not pick any color. Red will be used."
    fi

    echo "> All operations for initial setup are done!"
}

# Chose between dwm or Awesome
installTWM() {
    echo "> Do you want to install (d)wm or (a)wesome? dwm is recommended unless you have trouble compiling it."
    printf "> [d/a/N]? "
    read -r REPLY
    case "$REPLY" in
        d|D ) installDWM;;
        a|A ) installAWM;;
        * ) ;;
    esac
}

# Install dwm
installDWM() {
    echo "> Installing dwm-fawzadin."
    local crtdwm=$crtsrc/dwm-fawzadin
    [ -e "$crtdwm" ] || git clone https://gitlab.com/fawzadin/dwm-fawzadin.git $crtdwm
    cd $crtdwm

    [ "$installedSucklessDeps" = 0 ] && installSucklessDeps
   
    echo "> Copying important scripts to ~/.local/share/dwm"
    mkdir -p $HOME/.local/share/dwm
    cp -r dwmscripts/* $HOME/.local/share/dwm/
    [ -e "$HOME/.config/autostart.sh" ] && \
        mv $HOME/.local/share/dwm/autostart.sh $HOME/.local/share/dwm/autostart.bak.sh && \
        ln -sf $HOME/.config/autostart.sh $HOME/.local/share/dwm/autostart.sh

    # autocolor will automatically change the colorscheme
    if [ "$autocolor" = "blue" ]; then
        sed -i -e 's|BlueStart|BlueStart\*/|g' -e 's|BlueEnd|/\*BlueEnd|g' config.def.h  
        sed -i -e 's|RedStart\*/|RedStart|g' -e 's|/\*RedEnd|RedEnd|g' config.def.h
    elif [ "$autocolor" = "nord" ]; then
        sed -i -e 's|NordStart|NordStart\*/|g' -e 's|NordEnd|/\*NordEnd|g' config.def.h  
        sed -i -e 's|RedStart\*/|RedStart|g' -e 's|/\*RedEnd|RedEnd|g' config.def.h
    elif [ "$autocolor" = "red" ]; then
        : 
    else 
        echo "> You did not pick any color. Red will be used."
    fi
    
    # Automatically install st or Alacritty with setterm
    if [ "$setterm" = "0" ]; then
        echo "> Using st automatically."
    elif [ "$setterm" = "1" ]; then
        echo "> Using Alacritty automatically."
        patch -u -b config.def.h -i config.def.alacritty.patch
    else # If you somehow don't run initial() first.
        printf "> Which Terminal do you want to use? (a)lacritty or (s)t (a/S)? "
        read -r REPLY
        case $REPLY in 
            a|A ) echo "> Using Alacritty as terminal."
                patch -u -b config.def.h -i config.def.alacritty.patch
                ;;
            * ) echo "> Using st as terminal (default)."
                ;;
        esac 
    fi
    
    echo "> Installing dwm with make install"
    cp config.def.h config.h
    sudo make clean install
    
    #[ ! $? = 0 ] && echo "> Failed to install dwm. Try installing previously mentioned packages." && return 1
    echo "> All operations for dwm are done!"
}

# Install Awesome
installAWM() {
    cd $crt
    echo "> Installing Awesome Window Manager."
    if [ -x "$(command -v pacman)" ]; then 
        sudo pacman -S --noconfirm awesome
   elif [ -x "$(command -v apt)" ]; then
        sudo apt install -y awesome
    elif [ -x "$(command -v dnf)" ]; then
        sudo dnf install -y awesome
    else
        echo "> Your distro's package manager is not supported. You should install Awesome manually." && \
        return 1
    fi

    if [ "$autocolor" = "blue" ]; then
        sed -i -e '/chosen_theme/s/1/2/' $HOME/.config/awesome/rc.lua
    elif [ "$autocolor" = "nord" ]; then
        sed -i -e '/chosen_theme/s/1/3/' $HOME/.config/awesome/rc.lua
    elif [ "$autocolor" = "red" ]; then
        :
    else
        echo "> You did not pick any color. Red will be used."
    fi    

    sudo cp -f other/awesome.desktop /usr/share/xsessions/awesome.desktop

    echo "> All operations for Awesome WM are done!"
}


# Install Openbox
installOB() {
    confirm "> Do you want to install Openbox as your floating WM" || return 0
    cd $crt
    echo "> Installing Openbox."
    if [ -x "$(command -v pacman)" ]; then 
        sudo pacman -S --noconfirm openbox obconf lxappearance-obconf tint2 menumaker
        [ -x "$(command -v paru)" ] && paru -S --noconfirm pnmixer || \
        [ -x "$(command -v yay)" ] && yay -S --noconfirm pnmixer || \
        echo "> No AUR helper installed. Make sure to install pnmixer to add volume icon to systray." && \
        printf "> Press enter to continue... " && \
        read -r REPLY
    elif [ -x "$(command -v apt)" ]; then
        sudo apt install -y openbox obconf lxappearance-obconf tint2 pnmixer
    elif [ -x "$(command -v dnf)" ]; then
        sudo dnf install -y openbox obconf lxappearance-obconf tint2 pnmixer menumaker
    else
        echo "> Your distro's package manager is not supported. You should install Openbox and other packages manually." && \
        echo "> openbox obconf lxappearance-obconf tint2 pnmixer menumaker jgmenu"
        return 1
    fi

    echo "> Configuring Openbox."
    sudo cp -f other/openbox.desktop /usr/share/xsessions/openbox.desktop

    # Automatically install st or Alacritty with setterm
    if [ "$setterm" = "0" ]; then
        echo "> Using st automatically."
    elif [ "$setterm" = "1" ]; then
        echo "> Using Alacritty automatically."
        sed -i "s/>st/>alacritty/g" $HOME/.config/openbox/rc.xml
    else # If you somehow don't run initial() first.
        printf "> Which Terminal do you want to use? (a)lacritty or (s)t (a/S)? "
        read -r REPLY
        case $REPLY in 
            a|A ) echo "> Using Alacritty as terminal."
                sed -i "s/>st/>alacritty/g" $HOME/.config/openbox/rc.xml
                ;;
            * ) echo "> Using st as terminal (default)."
                ;;
        esac 
    fi

    if [ "$autocolor" = "blue" ]; then
        sed -i -e 's/#ef2929/#3465a4/g' -e 's/#262626/#22252c/g' $HOME/.config/tint2/tint2rc
        sed -i -e 's/Matcha-dark-alice/Matcha-dark-azure/g' $HOME/.config/openbox/rc.xml
    elif [ "$autocolor" = "nord" ]; then
        sed -i -e 's/#ef2929/#68759f/g' -e 's/#262626/#3b4252/g' $HOME/.config/tint2/tint2rc
        sed -i -e 's/Matcha-dark-alice/Nord-darker-standard-buttons/g' $HOME/.config/openbox/rc.xml
    elif [ "$autocolor" = "red" ]; then
        : 
    else 
        echo "> You did not pick any color. Red will be used."
    fi    

    echo "> All operations for Openbox are done!"
}


# Install Suckless Tools.
installSL() {
    echo "> Installing suckless tools (dmenu, slock, st, tabbed, dwmblocks)."
    local crtsl=$crtsrc/suckless-fawzadin
    [ -e "$crtsl" ] || git clone https://gitlab.com/fawzadin/suckless-fawzadin.git $crtsl

    [ "$installedSucklessDeps" = 0 ] && installSucklessDeps

    echo "> Installing dmenu."
    cd $crtsl/dmenu
    
    # autocolor will automatically change the colorscheme
    if [ "$autocolor" = "blue" ]; then
        sed -i -e 's|BlueStart|BlueStart\*/|g' -e 's|BlueEnd|/\*BlueEnd|g' config.def.h  
        sed -i -e 's|RedStart\*/|RedStart|g' -e 's|/\*RedEnd|RedEnd|g' config.def.h
    elif [ "$autocolor" = "nord" ]; then
        sed -i -e 's|NordStart|NordStart\*/|g' -e 's|NordEnd|/\*NordEnd|g' config.def.h  
        sed -i -e 's|RedStart\*/|RedStart|g' -e 's|/\*RedEnd|RedEnd|g' config.def.h
    elif [ "$autocolor" = "red" ]; then
        :
    else
        echo "> You did not pick any color. Red will be used."
    fi

    sudo make install
    
    # Slock may be broken on some distro.
    #echo "> Installing slock..."
    #cd $crtsl/slock
    #sudo make install
    
    echo "> Installing st."
    cd $crtsl/st

    if [ "$autocolor" = "blue" ]; then
        sed -i -e 's|#222222|#1b1d24|g' config.def.h  
    elif [ "$autocolor" = "nord" ]; then
        patch -b config.def.h color-nord.patch
    elif [ "$autocolor" = "red" ]; then
        :
    else
        echo "> You did not pick any color. Red will be used."
    fi

    sudo make install
    
    echo "> Installing tabbed."
    cd $crtsl/tabbed

    if [ "$autocolor" = "blue" ]; then
        sed -i -e 's|BlueStart|BlueStart\*/|g' -e 's|BlueEnd|/\*BlueEnd|g' config.def.h  
        sed -i -e 's|RedStart\*/|RedStart|g' -e 's|/\*RedEnd|RedEnd|g' config.def.h
    elif [ "$autocolor" = "nord" ]; then
        sed -i -e 's|NordStart|NordStart\*/|g' -e 's|NordEnd|/\*NordEnd|g' config.def.h  
        sed -i -e 's|RedStart\*/|RedStart|g' -e 's|/\*RedEnd|RedEnd|g' config.def.h
    elif [ "$autocolor" = "red" ]; then
        :
    else
        echo "> You did not pick any color. Red will be used."
    fi    

    sudo make install
    
    echo "> Installing dwmblocks."
    [ -e "$crtsl/dwmblocks-distrotube" ] || git clone https://gitlab.com/dwt1/dwmblocks-distrotube.git $crtsl/dwmblocks-distrotube
    cp -f $crtsl/blocks.def.h $crtsl/dwmblocks-distrotube/blocks.def.h
    cd $crtsl/dwmblocks-distrotube
    
    confirm "> Do you want to remove battery reading (recommended if you are on Desktop)" && \
        sed -i -e 13's/.*/\/\/&/' blocks.def.h
    
    confirm "> Do you want to remove temperature reading (recommended if you are on Virtual Machine)" && \
        sed -i -e 9's/.*/\/\/&/' blocks.def.h
    
    # Add -lX11 flag to Makefile
    sed -i -e '/pkg-config/s/$/ -lX11/' Makefile

    cp blocks.def.h blocks.h
    sudo make install
    
    echo "> All operations for suckless tools are done!"
}

# Install dmenuscripts for use with dmenu.
installDMS() {
    echo "> Installing dmscripts" 
    local crtdms=$crtsrc/dmscripts
    [ -e "$crtdms" ] || git clone https://gitlab.com/fawzadin/dmscripts.git $crtdms
    cd $crtdms

    echo "> Installing required depedencies."
    if [ -x "$(command -v pacman)" ]; then
        sudo pacman -S --noconfirm xclip findutils libnotify zenity xdotool xorg-xrandr jq
    elif [ -x "$(command -v apt)" ]; then
        sudo apt install -y xclip findutils libnotify-bin zenity xdotool x11-xserver-utils jq
    elif [ -x "$(command -v dnf)" ]; then
        sudo dnf install -y xclip findutils libnotify zenity xdotool xrandr jq
    else
        echo "> Your distro's package manager is not supported. Please install the following packages:" && \
        echo "> xclip xarg notify-send/libnotify zenity xdotool xrandr" && \
        printf "> Press enter to continue installing dmscripts or CTRL+C to quit..." && read
    fi

    echo "> Installing dmscripts."
    sudo make build install

    echo "> Copying config file to ~/.config/dmscripts"
    mkdir -p $HOME/.config/dmscripts
    cp $crtdms/config/config $HOME/.config/dmscripts/config

    # Set your terminal to alacritty
    [ "$setterm" = "1" ] && sed -i "/DMTERM/s/st/alacritty/" $HOME/.config/dmscripts/config

    echo "> All operations for dmscripts are done!"
}

# Download cool wallpapers
installWP() {
    confirm "> Download some cool Vaporwave and video game wallpapers" || return 0 
    cd $crt
    echo "> Downloading wallpapers to ~/Pictures/Wallpaper/"
    sh 3-wallpaper.sh
}

# Changing papirus folder color and improve font rendering
finishing() {
    echo "> Changing papirus folder color to black."
    if [ -x "$(command -v papirus-folders)" ]; then
        papirus-folders -C black
    else
        echo "> papirus-folders is not installed. Installing."
        wget -qO- https://git.io/papirus-folders-install | sh
        papirus-folders -C black
    fi
    
    # This part fixes the font rendering on Arch.
    # Taken from Manjaro wiki.
    # https://wiki.manjaro.org/index.php/Improve_Font_Rendering
    [ -x "$(command -v pacman)" ] || return
    echo "> Fix font rendering on Arch."
    sudo tee "/etc/fonts/local.conf" <<EOF 
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>
  <match target="font">
    <edit name="autohint" mode="assign">
      <bool>false</bool>
    </edit>
    <edit name="hinting" mode="assign">
      <bool>true</bool>
    </edit>
    <edit name="antialias" mode="assign">
      <bool>true</bool>
    </edit>
    <edit mode="assign" name="hintstyle">
      <const>hintslight</const>
    </edit>
    <edit mode="assign" name="rgba">
      <const>rgb</const>
    </edit>
    <edit mode="assign" name="lcdfilter">
      <const>lcddefault</const>
    </edit>
  </match>
</fontconfig>
EOF

    [ -e "$HOME/.Xresources" ] && cp $HOME/.Xresources $HOME/.Xresources.bak
    tee -a $HOME/.Xresources <<EOF
Xft.antialias: 1
Xft.hinting: 1
Xft.autohint: 0
Xft.rgba: rgb
Xft.hintstyle: hintslight
Xft.lcdfilter: lcddefault
EOF

    sudo ln -sf /usr/share/fontconfig/conf.avail/10-sub-pixel-rgb.conf /etc/fonts/conf.d/
    sudo ln -sf /usr/share/fontconfig/conf.avail/11-lcdfilter-default.conf /etc/fonts/conf.d/

    echo ""
    echo "> Please do the following steps manually to complete the font config:"
    echo "> 1. Run the following command after running your window manager:"
    echo "> $ xrdb -merge ~/.Xresources"
    echo "> (or run \"xmg\" in zsh shell)."
    echo "> 2. Open lxappearance and go to \"font\" tab."
    echo "> 3. Enable antialiasing."
    echo "> 4. Enable hinting and set to \"Slight\"."
    echo "> 5. Set Sub-pixel geometry to \"RGB\"."
    echo ""
}

# Don't run this with main. This is only for other functions.
installSucklessDeps() {
    echo "> Installing required depedencies."
    if [ -x "$(command -v pacman)" ]; then
        sudo pacman -S --noconfirm lm_sensors zenity
    elif [ -x "$(command -v apt)" ]; then
        sudo apt install -y build-essential libx11-dev lm-sensors libxinerama-dev sharutils libxft-dev libc6 libxcb-res0-dev libx11-xcb-dev libharfbuzz-dev zenity
    elif [ -x "$(command -v dnf)" ]; then
        sudo dnf install -y libX11-devel lm_sensors libXinerama-devel libXft-devel libxcb-devel zenity
    else
        echo "> Your distro's package manager is not supported. Please install the following packages/headers:" && \
        echo "> libx11 libxinerama libxft libxcb lm-sensors/lm_sensors zenity make gcc (or any C compiler)" && \
        printf "> Press enter to continue installing dwm/suckless tools or CTRL+C to quit..." && read
    fi
 
    installedSucklessDeps=1
}

main() {
    # Running all functions is recommended. You should not comment out initial. 
    # Also make sure the script to run the function in this exact order:
    # initial
    # pickColor
    # installTWM
    # installOB
    # installSL
    # installDMS
    # installWP
    # finishing

    initial
    pickColor
    installTWM
    installOB
    installSL
    installDMS
    installWP
    finishing

    echo "> All operations are done!"
    echo "> Please check init.vim of neovim config to change colorscheme if you didn't choose red (it is on the end of the config)."
    echo "> Run 'chsh -s /bin/zsh' to change your shell to zsh."
    echo "> If Flameshot still uses default config, import it from this git directory's other/flameshot.conf"
}

main
