-- This theme is based on "Multicolor" and other theme by lcpz
-- https://github.com/lcpz/awesome-copycats 

--================--
-- Load libraries -- 
--================--
local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

local os = os
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility
local markup = lain.util.markup

--================--
-- Theme Variable -- 
--================--
local theme                                     = {}

-- Basic
theme.confdir                                   = os.getenv("HOME") .. "/.config/awesome/themes/dwm-red"
theme.wallpaper                                 = theme.confdir .. "/wall.png"
theme.font                                      = "Fira Sans Regular 12"
theme.taglist_font                              = "Symbols Nerd Font 11"
theme.notification_font                         = "Fira Sans Regular 12"
theme.menu_font                                 = "Fira Sans Regular 12"
theme.tasklist_font                             = "Fira Sans Regular 12"

-- Color
theme.bg_normal                                 = "#22252c"
theme.bg_focus                                  = "#3465a4"
theme.bg_urgent                                 = "#ffffff"
theme.fg_normal                                 = "#d7d7d7"
theme.fg_focus                                  = "#d7d7d7"
theme.fg_urgent                                 = "#3465a4"
theme.fg_minimize                               = "#d7d7d7"

-- Border
theme.border_width                              = 2
theme.border_normal                             = "#22252c"
theme.border_focus                              = "#3465a4"
theme.border_marked                             = "#d7d7d7"

-- Menu
theme.menu_border_width                         = 0
theme.menu_width                                = 140
theme.menu_submenu_icon                         = theme.confdir .. "/icons/submenu.png"
theme.menu_fg_normal                            = "#d7d7d7"
theme.menu_fg_focus                             = "#ffffff"
theme.menu_bg_normal                            = "#22252c"
theme.menu_bg_focus                             = "#3465a4"

-- Widgets
theme.taglist_squares_sel                       = theme.confdir .. "/icons/square_a.png"
theme.taglist_squares_unsel                     = theme.confdir .. "/icons/square_b.png"
theme.tasklist_plain_task_name                  = true
theme.tasklist_disable_icon                     = true
theme.useless_gap                               = 4
theme.gap_single_client                         = false

-- Layout txt
theme.layout_txt_tile                           = "[]≡"
theme.layout_txt_tileleft                       = "≡[]"
theme.layout_txt_tilebottom                     = "TTT"
theme.layout_txt_tiletop                        = "==="
theme.layout_txt_fairv                          = "[fv]"
theme.layout_txt_fairh                          = "[fh]"
theme.layout_txt_spiral                         = "[@]"
theme.layout_txt_dwindle                        = "[\\\\]"
theme.layout_txt_max                            = "(M)"
theme.layout_txt_fullscreen                     = "[F]"
theme.layout_txt_magnifier                      = "[M]"
theme.layout_txt_floating                       = "(~)"

-- lain related
theme.layout_txt_cascade                        = "[c]"
theme.layout_txt_cascadetile                    = "[ct]"
theme.layout_txt_centerwork                     = "|M|"
theme.layout_txt_termfair                       = "[tf]"
theme.layout_txt_centerfair                     = "[cf]"


--=================--
-- Widget Settings -- 
--=================--

-- Separator
local sprtr = wibox.widget.textbox("/") -- alternative: ❬
sprtr.font = "Fira Sans Regular 11" -- fix separators that might become small
local empty = wibox.widget.textbox("   ")
local sprtricon = wibox.widget.imagebox(theme.confdir .. "/icons/darkbar.png")

-- Textclock
os.setlocale(os.getenv("LANG")) -- to localize the clock
local mytextclock = wibox.widget.textclock(markup(theme.fg_normal, "  %a %b %d, %H:%M "))
mytextclock.font = theme.font

-- CPU
local cpu = lain.widget.cpu({
    timeout = 5,
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, theme.fg_normal, " " .. cpu_now.usage .. "% "))
    end
})

-- Coretemp
local temp = lain.widget.temp({
    timeout = 5,
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, theme.fg_normal, " +" .. coretemp_now .. "°C "))
    end
})

-- Battery
local bat = lain.widget.bat({
    battery = "BAT0",
    timeout = 5,
    settings = function()
        local perc = bat_now.perc ~= "N/A" and bat_now.perc .. "%" or bat_now.perc

        local cap = " "

        if bat_now.perc == "N/A" then 
            cap = "  "
        elseif bat_now.ac_status == 0 and bat_now.perc <= 20 then
            cap = "  "
        elseif bat_now.ac_status == 0 and bat_now.perc > 20 and bat_now.perc <= 40 then
            cap = "  "
        elseif bat_now.ac_status == 0 and bat_now.perc > 40 and bat_now.perc <= 60 then
            cap = "  "
        elseif bat_now.ac_status == 0 and bat_now.perc > 60 and bat_now.perc <= 80 then
            cap = "  "
        elseif bat_now.ac_status == 0 and bat_now.perc >= 80 then
            cap = "  "
        elseif bat_now.status == "N/A" or bat_now.status == nil then
            cap = " "
        elseif bat_now.status == "Not charging" or bat_now.status == "Full" then
            cap = " "
        elseif bat_now.status == "Charging" then
            cap = ""
        end

        perc = " " .. cap .. perc .. " "

        widget:set_markup(markup.fontfg(theme.font, theme.fg_normal, perc))
    end
})

-- ALSA volume
theme.volume = lain.widget.alsa({
    settings = function()
        local vol = "奔 "
        local volcheck = tonumber(volume_now.level)

        if volcheck <= 30 then
            vol = "奄 "
        elseif volcheck >= 70 then
            vol = "墳 "
        end

        volume_now.level = vol .. volume_now.level .. "% "

        if volume_now.status == "off" then
            volume_now.level = "婢 ------ "
        end

        widget:set_markup(markup.fontfg(theme.font, theme.fg_normal, volume_now.level))
    end
})


-- MEM
local memory = lain.widget.mem({
    timeout = 5,
    settings = function()
        local totalg = math.floor(mem_now.total / 1024 * 10) / 10
        local bytem = "Mi"

        if mem_now.used >= 1000 then
            mem_now.used = math.floor(mem_now.used / 1024 * 10) / 10
            bytem = "Gi"
        end

        widget:set_markup(markup.fontfg(
        theme.font, 
        theme.fg_normal, 
        "  " .. mem_now.used .. bytem .. "/" .. totalg .. "Gi "
        ))
    end
})


--================--
-- Wibox Settings -- 
--================--

-- Writes a string representation of the current layout in a textbox widget
function update_txt_layoutbox(s)
    local txt_l = theme["layout_txt_" .. awful.layout.getname(awful.layout.get(s))] or ""
    s.mytxtlayoutbox:set_text(txt_l)
end

-- Now we put every widget to the screen
function theme.at_screen_connect(s)
    -- Quake application
    --s.quake = lain.util.quake({ app = "pavucontrol" })
    --s.quake = lain.util.quake({ app = "alacritty", height = 0.50, argname = "-e htop" })

    -- If wallpaper is a function, call it with the screen
    local wallpaper = theme.wallpaper
    if type(wallpaper) == "function" then
        wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)

    -- Tags
    awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()

    -- Textual layoutbox
    s.mytxtlayoutbox = wibox.widget.textbox(theme["layout_txt_" .. awful.layout.getname(awful.layout.get(s))])
    awful.tag.attached_connect_signal(s, "property::selected", function () update_txt_layoutbox(s) end)
    awful.tag.attached_connect_signal(s, "property::layout", function () update_txt_layoutbox(s) end)
    s.mytxtlayoutbox:buttons(my_table.join(
                           awful.button({}, 1, function() awful.layout.inc(1) end),
                           awful.button({}, 2, function () awful.layout.set( awful.layout.layouts[1] ) end),
                           awful.button({}, 3, function() awful.layout.inc(-1) end),
                           awful.button({}, 4, function() awful.layout.inc(1) end),
                           awful.button({}, 5, function() awful.layout.inc(-1) end)))

    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons, tasklist_style)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "bottom", screen = s, height = 22, bg = theme.bg_normal, fg = theme.fg_normal })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist,
            empty,
            s.mytxtlayoutbox,
            s.mypromptbox,
            empty,
        },
        -- Middle widget
        s.mytasklist,         
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            empty,
            cpu.widget,
            sprtr,
            memory.widget,
            sprtr,
            temp.widget,
            sprtr,
            theme.volume.widget,
            sprtr,
            bat.widget,
            sprtr,
            mytextclock,
            sprtricon,
            wibox.widget.systray(),
        },
    }

end

return theme
