# Defined in - @ line 1
function fstabcat --wraps='cat /etc/fstab' --description 'alias fstabcat=cat /etc/fstab'
  cat /etc/fstab $argv;
end
