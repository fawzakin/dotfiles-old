#!/usr/bin/env -S bash -e
# archsetup.sh: Quickly install Arch from any Arch system. 
# Forked from classy-girrafe's easy-arch.
# https://github.com/classy-giraffe/easy-arch
# License: MIT License 
#TODO: Artix Support, Installing my config file through this installer.


#======================#
#||   Initial Setup  ||#
#======================#

# Set some flag for safety
#set -o nounset # error when referencing undefined variable.
set -o errexit # exit when a command fails.

# Quit immediately if you don't have arch-install-scripts or trying to run this outside Arch.
[ ! -f "/etc/arch-release" ] || [ ! -x "$(command -v pacstrap)" ] && \
    echo "> Please run this on Arch ISO or existing Arch system with arch-install-scripts installed" \
    "(Artix is not currently supported)." && exit 1

# Ask the user if using an automatic installer is a good idea.
read -r -p "> WARNING: Automatic install script may damage your system. Continue [y/N]? " reply
[[ ! "$reply" =~ ^(y|Y)$ ]] && exit 1

# Checking the microcode to install.
CPU=$(grep vendor_id /proc/cpuinfo)
if [[ $CPU == *"AuthenticAMD"* ]]
then
    microcode=amd-ucode
else
    microcode=intel-ucode
fi


#============================#
#||   Defining Functions   ||#
#============================#

# Preparation before installing Arch.
prepare_install() {
    echo "> Syncing current time."
    timedatectl set-ntp true

    echo "> Changing mirrorlist to latest 10 mirrors sorted by download speed."
    echo "> This may take a while if reflector encounters bad mirros."
    reflector --latest 10 --protocol https --sort rate --save /etc/pacman.d/mirrorlist &>/dev/null

    echo "> Syncing pacman repo."
    pacman -Syy
}

# Loop through cfdisk. Exit on confirm. 
# We use while loop to prevent a bug where the installer would quit unexpectedly with recursive loop.
cfdisk_loop() {
    while :; do
        cfdisk
        read -r -p "> Continue (Enter N to run cfdisk again) [y/N]? " reply
        [[ "$reply" =~ ^(y|Y)$ ]] && break
    done
}

# Selecting a kernel to install. 
kernel_selector () {
    while :; do
        echo ""
        echo "> List of kernels:"
        echo "> 1) Stable — Vanilla Linux kernel and modules, with a few patches applied."
        echo "> 2) Longterm — Long-term support (LTS) Linux kernel and modules."
        echo "> 3) Hardened — A security-focused Linux kernel."
        echo "> 4) Zen — Optimized for desktop/gaming usage."
        read -r -p "> Insert the number of the corresponding kernel: " choice
        echo "> Choice $choice will be installed"
        case "$choice" in
            1 ) kernel=linux
                break
                ;;
            2 ) kernel=linux-lts
                break
                ;;
            3 ) kernel=linux-hardened
                break
                ;;
            4 ) kernel=linux-zen
                break
                ;;
            * ) echo "You did not enter a valid selection."
                continue
                ;;
        esac
    done
}

# Selecting a way to handle internet connection. 
network_selector () {
    while :; do
        echo ""
        echo "> Network utilities:"
        echo "> 1) IWD — iNet wireless daemon is a wireless daemon for Linux written by Intel (WiFi-only)."
        echo "> 2) NetworkManager — Program for providing detection and configuration for systems to automatically connect to networks (both WiFi and Ethernet)."
        echo "> 3) wpa_supplicant — It's a cross-platform supplicant with support for WEP, WPA and WPA2 (WiFi-only, a DHCP client will be automatically installed too.)"
        echo "> 4) I will do this on my own."
        read -r -p "> Insert the number of the corresponding networking utility: " choice
        echo "> Choice $choice will be installed"
        case "$choice" in
            1 ) echo "> Installing IWD."    
                pacstrap /mnt iwd
                echo "> Enabling IWD."
                systemctl enable iwd --root=/mnt &>/dev/null
                break
                ;;
            2 ) echo "> Installing NetworkManager."
                pacstrap /mnt networkmanager
                echo "> Enabling NetworkManager."
                systemctl enable NetworkManager --root=/mnt &>/dev/null
                break
                ;;
            3 ) echo "> Installing wpa_supplicant and dhcpcd."
                pacstrap /mnt wpa_supplicant dhcpcd
                echo "> Enabling wpa_supplicant and dhcpcd."
                systemctl enable wpa_supplicant --root=/mnt &>/dev/null
                systemctl enable dhcpcd --root=/mnt &>/dev/null
                break
                ;;
            4 ) break
                ;;
            * ) echo "> You did not enter a valid selection."
                continue
                ;;
        esac
    done
}


#==========================#
#||   Pre-Installation   ||#
#==========================#

# Start the install with preparation.
read -r -p "> Start preparation (Syncing time with network and updating mirrors) [y/N]? " reply
[[ "$reply" =~ ^(y|Y)$ ]] && prepare_install

# Let users make their own partition. 
# Usefull if they want to dual boot.
clear
lsblk

echo ""
echo "> Make sure you have at least two partitions for BOOT and ROOT"
echo "> (Three if you want separated HOME)."
echo "> If you do not have those, please run cfdisk."
echo ""

read -r -p "> Run cfdisk [y/N]? " reply
[[ "$reply" =~ ^(y|Y)$ ]] && cfdisk_loop

while :; do
    # Selecting the target for the installation.
    while :; do
        clear
        lsblk

        WITH_HOME=0 

        printf "\n> Tips: Select the partition you want based on its size.\n\n"
        read -r -p "> Do you want to have separate home partiton [y/N]? " reply
        [[ "$reply" =~ ^(y|Y)$ ]] && WITH_HOME=1 

        PS3="> Select the disk where Arch Linux is going to be installed: "
        select ENTRY in $(lsblk -dpnoNAME | grep -P "/dev/sd|nvme|vd"); do
            DISK=$ENTRY
            break
        done

        if [ "$WITH_HOME" = 0 ] && [ "$(lsblk -rpnoNAME $DISK | wc -l)" -lt "3" ]; then
            echo "> WARNING: The selected disk doesn't have 2 partitions at minimum."
            read -r -p "> Run cfdisk (Enter N to pick other disk) [y/N]? " reply
            [[ "$reply" =~ ^(y|Y)$ ]] && cfdisk_loop && continue || continue 
        elif [ "$WITH_HOME" = 1 ] && [ "$(lsblk -rpnoNAME $DISK | wc -l)" -lt "4" ]; then
            echo "> WARNING: The selected disk doesn't have 3 partitions at minimum."
            read -r -p "> Run cfdisk (Enter N to pick other disk) [y/N]? " reply
            [[ "$reply" =~ ^(y|Y)$ ]] && cfdisk_loop && continue || continue 
        fi

        echo "> Installing Arch Linux on $DISK."
        break
    done

    PS3="> Select the partition for EFI, the boot partition (if it's already FAT32, it will NOT be formatted): "
    select ENTRY in $(lsblk -pnoNAME -xNAME $DISK | awk '{if(NR>1)print}'); do
        BOOTSEL=$ENTRY
        break
    done
    
    PS3="> Select the partition for ROOT, the main filesystem: "
    select ENTRY in $(lsblk -pnoNAME -xNAME $DISK | awk '{if(NR>1)print}' | grep $BOOTSEL -v); do
        ROOTSEL=$ENTRY
        break
    done

    if [ "$WITH_HOME" = 1 ]; then
        PS3="> Select the partition for HOME, your home directory: "
        select ENTRY in $(lsblk -pnoNAME -xNAME $DISK | awk '{if(NR>1)print}' | grep -E "$BOOTSEL|$ROOTSEL" -v); do
            HOMESEL=$ENTRY
            break
        done
    fi
    
    echo ""
    echo "> EFI partition: $BOOTSEL (/boot/efi)"
    echo "> ROOT partition: $ROOTSEL (/)"
    [ "$WITH_HOME" = 1 ] && echo "> HOME partition: $HOMESEL (/home)"

    read -r -p "> Are you sure you this is corrent (Enter N to pick again) [y/N]? " reply
    [[ "$reply" =~ ^(y|Y)$ ]] && break
done

# Formatting the BOOTSEL as FAT32 only if it's not formatted as such.
clear
echo "> The program will now format any selected partitons."
echo "> None of your selected disk is modified in anyway before formatting starts (unless you ran cfdisk)."
echo "> This is your last chance to check your selected disks before all of its existing data be destroyed."
echo "> Select N to any query that asks for formatting the disk to quit this program."
echo ""
echo "> EFI partition: $BOOTSEL (/boot/efi)"
echo "> ROOT partition: $ROOTSEL (/)"
[ "$WITH_HOME" = 1 ] && echo "> HOME partition: $HOMESEL (/home)"
echo ""

if [ ! "$(lsblk -noFSTYPE $BOOTSEL)" = "vfat" ]; then
    echo "> EFI partition is not FAT32."
    read -r -p "> Do you want to format it to FAT32 [y/N]? " reply
    if [[ "$reply" =~ ^(y|Y)$ ]]; then 
        echo "> Formatting the EFI partition as FAT32."
        mkfs.fat -F 32 $BOOTSEL &>/dev/null 
    else
        echo "> Quitting installer." && exit 1
    fi
fi

echo ""

# Formatting the ROOTSEL and HOMESEL as BTRFS.
read -r -p "> Do you want to format ROOT$([ "$WITH_HOME" = 1 ] && echo " and HOME") partition [y/N]? " reply
[[ ! "$reply" =~ ^(y|Y)$ ]] && echo "Quitting installer." && exit 1

echo "> Formatting the ROOT partition as BTRFS."
mkfs.btrfs -f $ROOTSEL &>/dev/null

[ "$WITH_HOME" = 1 ] && echo "> Formatting the HOME partition as BTRFS." && \
mkfs.btrfs -f $HOMESEL &>/dev/null

echo ""

# Informing the Kernel of the changes.
echo "> Informing the Kernel about the disk changes."
partprobe "$DISK"

# Creating BTRFS subvolumes.
echo "> Creating BTRFS subvolumes."
mount $ROOTSEL /mnt
btrfs su cr /mnt/@ &>/dev/null

if [ "$WITH_HOME" = 0 ]; then
    btrfs su cr /mnt/@home &>/dev/null
elif [ "$WITH_HOME" = 1 ]; then
    mkdir -p /mnt/tmp
    mount $HOMESEL /mnt/tmp
    btrfs su cr /mnt/tmp/@home &>/dev/null
    umount /mnt/tmp
    rmdir /mnt/tmp
fi

btrfs su cr /mnt/@var_log &>/dev/null
btrfs su cr /mnt/@var_cache &>/dev/null
btrfs su cr /mnt/@snapshots &>/dev/null

# Mounting the newly created subvolumes.
umount /mnt

echo "> Mounting the newly created subvolumes."
mount -o ssd,noatime,space_cache=v2,compress=zstd,subvol=@ $ROOTSEL /mnt
mkdir -p /mnt/{home,/boot/efi,/var/log,/var/cache,.snapshots}
[ "$WITH_HOME" = 0 ] && mount -o ssd,noatime,space_cache=v2,compress=zstd,autodefrag,discard=async,subvol=@home $ROOTSEL /mnt/home
[ "$WITH_HOME" = 1 ] && mount -o ssd,noatime,space_cache=v2,compress=zstd,autodefrag,discard=async,subvol=@home $HOMESEL /mnt/home
mount -o ssd,noatime,space_cache=v2,compress=zstd,autodefrag,discard=async,subvol=@var_log $ROOTSEL /mnt/var/log
mount -o ssd,noatime,space_cache=v2,compress=zstd,autodefrag,discard=async,subvol=@var_cache $ROOTSEL /mnt/var/cache
mount -o ssd,noatime,space_cache=v2,compress=zstd,autodefrag,discard=async,subvol=@snapshots $ROOTSEL /mnt/.snapshots
chattr +C /mnt/var/log
chattr +C /mnt/var/cache
mount $BOOTSEL /mnt/boot/efi

# Pacstrap (setting up a base sytem onto the new root).
kernel_selector

echo "> Installing the base system (this may take a while)."
pacstrap /mnt base base-devel $kernel ${kernel}-headers $microcode linux-firmware \
    btrfs-progs ntfs-3g dosfstools grub grub-btrfs efibootmgr os-prober sudo polkit reflector snapper git neovim xdg-user-dirs

network_selector


#===========================#
#||   Post-Installation   ||#
#===========================#

# Generating /etc/fstab.
echo "> Generating a new fstab."
genfstab -U /mnt >> /mnt/etc/fstab

# Setting hostname.
echo ""
echo "> Please double check the entry before pressing enter!"
read -r -p "> Enter the hostname: " hostname
echo "$hostname" > /mnt/etc/hostname

# Setting username.
read -r -p "> Enter your username: " username
echo "> You can add more by running 'useradd -m [USERNAME]' in chroot."

# Setting up locales.
read -r -p "> Please insert the main locale you want to use (format: xx_XX eg: en_US | enter empty to use en_US): " locale
if [ -n "$locale" ]; then
    echo "$locale.UTF-8 UTF-8" > /mnt/etc/locale.gen
    echo "LANG=$locale.UTF-8" > /mnt/etc/locale.conf
else
    echo "en_US.UTF-8 UTF-8" > /mnt/etc/locale.gen
    echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf
fi

# Setting up keyboard layout.
read -r -p "> Please insert the keyboard layout you use (enter empty to use default layout): " kblayout
[ -n "$kblayout" ] && echo "KEYMAP=$kblayout" > /mnt/etc/vconsole.conf

# Setting hosts file.
echo "> Setting hosts file."
cat > /mnt/etc/hosts <<EOF
127.0.0.1   localhost
::1         localhost
127.0.1.1   $hostname.localdomain   $hostname
EOF

# Configuring /etc/mkinitcpio.conf.
echo "> Configuring /etc/mkinitcpio.conf for BTRFS hook (and NVIDIA if you have it)."
if lspci | grep -q NVIDIA; then
    sed -i -e 's|MODULES=()|MODULES=(btrfs nvidia)|g' /mnt/etc/mkinitcpio.conf
else
    sed -i -e 's|MODULES=()|MODULES=(btrfs)|g' /mnt/etc/mkinitcpio.conf
fi

# Configuring the system.    
arch-chroot /mnt /bin/bash -e <<EOF
    
    # Setting up timezone.
    echo "> Setting up timezone and clock."
    ln -sf /usr/share/zoneinfo/$(curl -s http://ip-api.com/line?fields=timezone) /etc/localtime &>/dev/null
    
    # Setting up clock.
    hwclock --systohc

    # Generating locales.
    echo "> Generating locales."
    locale-gen &>/dev/null

    # Generating a new initramfs.
    echo "> Creating a new initramfs. This may take a while."
    mkinitcpio -P &>/dev/null

    # Snapper configuration
    echo "> Making snapper config."
    umount /.snapshots
    rm -r /.snapshots
    snapper --no-dbus -c root create-config /
    btrfs subvolume delete /.snapshots &>/dev/null
    mkdir /.snapshots
    mount -a
    chmod 750 /.snapshots

    echo "> Configuring snapper."
    sed -i "/TIMELINE_LIMIT_HOURLY/s/=.*$/=\"0\"/" /etc/snapper/configs/root
    sed -i "/TIMELINE_LIMIT_DAILY/s/=.*$/=\"3\"/" /etc/snapper/configs/root
    sed -i "/TIMELINE_LIMIT_WEEKLY/s/=.*$/=\"3\"/" /etc/snapper/configs/root
    sed -i "/TIMELINE_LIMIT_MONTHLY/s/=.*$/=\"1\"/" /etc/snapper/configs/root
    sed -i "/TIMELINE_LIMIT_YEARLY/s/=.*$/=\"0\"/" /etc/snapper/configs/root

    # Installing GRUB.
    echo "> Installing GRUB on /boot."
    echo "GRUB_DISABLE_OS_PROBER=\"false\"" >> /etc/default/grub
    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB &>/dev/null
    
    # Creating grub config file.
    echo "> Creating GRUB config file."
    grub-mkconfig -o /boot/grub/grub.cfg &>/dev/null

    # Adding user with sudo privileges
    echo "> Adding ${username}."
    useradd -m $username
    usermod -aG wheel $username
    echo "$username ALL=(ALL) ALL" >> /etc/sudoers.d/$username

    # Configuring other stuff
    echo "> Disabling PC speaker sound."
    echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf

    echo "> Configuring pacman."
    grep -q "^Color" /etc/pacman.conf || sed -i "/#Color/s/^#//" /etc/pacman.conf
    grep -q "^ParallelDownloads" /etc/pacman.conf || sed -i "/#ParallelDownloads/s/^#//" /etc/pacman.conf
    grep -q "ILoveCandy" /etc/pacman.conf || sed -i "/#VerbosePkgLists/a ILoveCandy" /etc/pacman.conf

EOF

# Setting root and user password.
echo ""
echo "> Add password for the root account."
arch-chroot /mnt /bin/passwd

echo ""
echo "> Add password for the user account (${username})." 
arch-chroot /mnt /bin/passwd $username

# Enabling remaining services.
echo "> Enabling remaining services."
systemctl enable reflector.timer --root=/mnt &>/dev/null
systemctl enable snapper-timeline.timer --root=/mnt &>/dev/null
systemctl enable snapper-cleanup.timer --root=/mnt &>/dev/null
systemctl enable grub-btrfs.path --root=/mnt &>/dev/null

echo ""
echo "> Done, you may now reboot your system (further changes can be done by chrooting into /mnt)."
echo "> NOTE: The installed text editor is Neovim (run with nvim)."
read -r -p "> Chroot now [y/N]? " reply
[[ "$reply" =~ ^(y|Y)$ ]] && arch-chroot /mnt
echo "> Unmounting partition."
umount -a &>/dev/null
echo "> Reboot the system once you are ready."

exit 0
