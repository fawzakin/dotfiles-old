### Next
- [ ] Use GNU Stow instead of copying
- [ ] Xresource support
- [ ] Script to change colorscheme post-install
- [ ] Artix support (OpenRC only)

### Done
- [x] Separate lightdm to its own function for use with startx
- [x] Test the new 0-initial.sh
- [x] Integrate 0-initial.sh with [classy-girrafe's easy-arch](https://github.com/classy-giraffe/easy-arch)
- [x] Remove kitty (but keep config)
- [x] Add Fedora support
- [x] Fix Debian/Ubuntu support (will be polished on Debian 11)
