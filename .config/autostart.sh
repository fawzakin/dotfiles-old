#!/bin/sh
# This autostart works across different window manager (dwm, openbox, awesome).
# It has been sys-linked to respective WM's config directory so you only need to edit
# the one autostart.sh in ~/.config.

# Uncomment for VM purpose
#xrandr -s 1280x720
#xrandr -s 1366x768
#xrandr -s 1920x1080
#nitrogen --restore &

# Always put "&" at the end of processes you want to start.
#lxsession &
lxpolkit &
picom --vsync &
nm-applet &
killall redshift; killall redshift-gtk; redshift-gtk & # Do NOT turn on autostart for redshift

# Start up MPD and friends for Radio
mpd &
mpDris2 &

# Only when dwm is running.
if pgrep -x dwm > /dev/null; then
    killall dwmblocks
    while pgrep -u $UID -x dwmblocks >/dev/null; do sleep 1; done
    dwmblocks &
    [ -f "$HOME/.cache/dwmcs" ] && rm -f $HOME/.cache/dwmcs # Remove cheatsheet cache
    [ -e "$HOME/.config/xmodmap" ] || cp -f $HOME/.local/share/dwm/xmodmap $HOME/.config/xmodmap
fi

# Only when Openbox is running.
if pgrep -x openbox > /dev/null; then
    tint2 &
    pnmixer &
fi

# Only when Awesome is running.
if pgrep -x awesome > /dev/null; then
    :
fi

# Modify this if you like "natural scrolling" on laptop.
xinput set-prop "ETPS/2 Elantech Touchpad" "libinput Natural Scrolling Enabled" 1 

# set caps lock to right super key as mod3 since it is a useless key anyway.
# Install xcape to send escape key from caps lock when releasing it under few milliseconds.
xmodmap $HOME/.config/xmodmap  
xcape -t 200 -e 'Super_R=Escape'

# Check for update every friday. Only works on Arch.
# Make sure to put this on the last line.
[ -x "$(command -v checkupdates)" ] && [ $(date +"%a") = "Fri" ] && upnum=$(checkupdates | wc -l) && [ $upnum -ge 5 ] && \
notify-send -u normal "Update Friday" "$upnum packages to be updated."

echo "done" # For debugging purpose

