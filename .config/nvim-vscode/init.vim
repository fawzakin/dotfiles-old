" Some settings are taken from the following sources:
" https://vim.fandom.com/wiki/Example_vimrc
" https://www.chrisatmachine.com/Neovim/01-vim-plug/
" https://www.chrisatmachine.com/Neovim/02-vim-general-settings/

" Source settings
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.config/nvim/settings.vim

" Source custom keybindings
source ~/.config/nvim/keybindings.vim

" Source plugins
" Using vim-plug because I can't figure out how to install plugins manually.
" Install vim-plug and run :PlugInstall
source ~/.config/nvim/plugins.vim

" Source GUI configs
" For Neovim GUI wrappers
source ~/.config/nvim/gui.vim

