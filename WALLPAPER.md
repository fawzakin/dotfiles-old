Here are some good wallpaper I'd like to use. 
This document lists out links of wallpapers that cannot be downloaded directly with curl.

Custom wallpaper:
+ [Google Drive](https://drive.google.com/drive/folders/1489PbyXlqyZe1U0Dw01DRE8FJkwL3tLi?usp=sharing)
 
Minimal Gruvbox lines with distro Logo by reddit user u/atlas-ark:
+ [Arch](https://drive.google.com/drive/u/0/mobile/folders/1orOcryRZajp0pxSZLkwM91Gt1OXxQIWB?usp=sharing)
+ [Manjaro](https://drive.google.com/drive/u/0/mobile/folders/1f6Ln3H674d7dXtKN-9YfDJ5fMCJjn-hR?usp=sharing)
+ [Debian](https://drive.google.com/drive/u/0/mobile/folders/1JnScjcwozzlbh33mh0NNMxNc12_SLC5j?usp=sharing)
+ [Ubuntu](https://drive.google.com/drive/u/0/mobile/folders/19t9wm-iC-TnCjd4--3RxWPWU4MCzGmPE?usp=sharing)
+ [Pop OS](https://drive.google.com/drive/u/0/mobile/folders/1j40La-aVOXidNeHytdtM_N-lM3OFwafZ?usp=sharing)
+ [Mint](https://drive.google.com/drive/u/0/mobile/folders/1Vx9zXEX9dfjFOVd_qwMrjnqxgZDPBoLb?usp=sharing)
+ [Fedora](https://drive.google.com/drive/u/0/mobile/folders/1K4hFpzeD6tgpoSok3BxzA6-4wfOOKl9I?usp=sharing)
+ [Linux](https://drive.google.com/drive/u/0/mobile/folders/1HNaVJiyJMSVpenQ6XrR__sZ7474F4prz?usp=sharing)
+ [Simple Line](https://drive.google.com/drive/u/0/mobile/folders/1zH_f6Hao0ko4WxrRq4G-8koSTaZ6Wucn?usp=sharing)
+ [Rest](https://www.reddit.com/r/wallpaper/comments/kcffkg/1920x1080_penguin_linux_wallpaper_dark_light_svg/?utm_source=share)
