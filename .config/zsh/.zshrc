# My config of zsh.
# Some of this lines are ripped off from Luke Smith's and Brodie Robertson's zsh config.
# Don't forget to install zsh-syntax-highlighting, zsh-autosuggestions, spaceship-prompt.

# Enable colors
autoload -U colors && colors	

# History variables
HISTFILE=~/.cache/zsh/zsh_history
HISTSIZE=10000
SAVEHIST=10000
export HISTCONTROL=ignoredups

# Load aliases
# Edit aliases by running "zalias"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.zalias" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.zalias"

# Auto completion
autoload -U compinit && compinit -u
zstyle ':completion:*' menu select

# Auto complete with case insenstivity
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# Include hidden files
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# Vim mode
bindkey -v
export KEYTIMEOUT=1

# Enable searching through history
bindkey '^R' history-incremental-pattern-search-backward

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# And arrow keys as well:
bindkey -M menuselect 'left' vi-backward-char
bindkey -M menuselect 'down' vi-down-line-or-history
bindkey -M menuselect 'up' vi-up-line-or-history
bindkey -M menuselect 'right' vi-forward-char

# Fix bug when switching mode
bindkey -v '^?' backward-delete-char

# Edit line in vim buffer ctrl-v
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Enter vim buffer from normal mode
autoload -U edit-command-line && zle -N edit-command-line && bindkey -M vicmd "^e" edit-command-line

# Change cursor shape for different vi modes.
function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';;      # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Spaceship prompts
SPACESHIP_PROMPT_ADD_NEWLINE=false
SPACESHIP_PROMPT_SEPARATE_LINE=false
SPACESHIP_CHAR_SYMBOL=\>
SPACESHIP_CHAR_SUFFIX=" "
SPACESHIP_CHAR_COLOR_SUCCESS=green
SPACESHIP_DIR_TRUNC_PREFIX="../"
SPACESHIP_DIR_COLOR=blue
SPACESHIP_EXEC_TIME_SHOW=false
SPACESHIP_EXIT_CODE_SHOW=true
SPACESHIP_EXIT_CODE_SYMBOL="✘ "
SPACESHIP_GIT_STATUS_DELETED=x
SPACESHIP_GIT_STATUS_COLOR=yellow
SPACESHIP_HG_SHOW=false
SPACESHIP_PACKAGE_SHOW=false
SPACESHIP_NODE_SHOW=false
SPACESHIP_RUBY_SHOW=false
SPACESHIP_ELM_SHOW=false
SPACESHIP_ELIXIR_SHOW=false
SPACESHIP_XCODE_SHOW_LOCAL=false
SPACESHIP_SWIFT_SHOW_LOCAL=false
SPACESHIP_GOLANG_SHOW=false
SPACESHIP_PHP_SHOW=false
SPACESHIP_RUST_SHOW=false
SPACESHIP_JULIA_SHOW=false
SPACESHIP_DOCKER_SHOW=false
SPACESHIP_DOCKER_CONTEXT_SHOW=false
SPACESHIP_AWS_SHOW=false
SPACESHIP_CONDA_SHOW=false
SPACESHIP_VENV_SHOW=false
SPACESHIP_PYENV_SHOW=false
SPACESHIP_DOTNET_SHOW=false
SPACESHIP_EMBER_SHOW=false
SPACESHIP_KUBECONTEXT_SHOW=false
SPACESHIP_TERRAFORM_SHOW=false
SPACESHIP_TERRAFORM_SHOW=false
SPACESHIP_VI_MODE_SHOW=false
SPACESHIP_JOBS_SHOW=false

autoload -U promptinit; promptinit
prompt spaceship

# Keybinding for programs
bindkey -s "^k" "clear\n"

# Other
unsetopt beep		# Disable PC speaker sound
setopt autocd		# Automatically cd into typed directory.
stty stop undef		# Disable ctrl-s to freeze terminal.
setopt interactive_comments

# Load usefull zsh plugins
for zshplug in /usr/share/zsh/plugins /usr/share; do
    # Syntax Highlighting
    [ -f ${zshplug}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && \
    source ${zshplug}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
    # Autosuggestion
    [ -f ${zshplug}/zsh-autosuggestions/zsh-autosuggestions.zsh ] && \
    source ${zshplug}/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null && break
done

# Command not found
#source /usr/share/doc/pkgfile/command-not-found.zsh 2>/dev/null
