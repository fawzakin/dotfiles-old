# Polybar Config
This config is partially unfinished and i'm not going to finish it anytime soon.
This is because it doesn't work well with the window manager I'm using (it is suited for i3 and bspwm).

For instace, the systray will always be placed on top even if there's any fullscreen application. This behavior has been reported as [a bug for 4 years](https://github.com/polybar/polybar/issues/425) and the fix only works with the aformentioned WMs.  

Even if the systray is working, I don't like the usage of it. I prefer to have traditional Windows-like usage for floating-window manager (like what I have with the current Openbox/Tint2 settings) compared to whatever the workflow I make myself within this config.

If you want to use this, I recommend to install either `xfce4-panel` or `plank` and have the openbox decoration disabled (drag window by holding the MOD key and drag around with left click).
