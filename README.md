# THIS REPO IS DEPRECATED
Please use the new and improved dotfiles repo that uses GNU `stow`.
[Click here](https://gitlab.com/fawzakin/dotfiles)

# Dotfiles

A collection of my personal Arch Linux settings. They should work on other Linux distro.

# Installer

Here are the scripts to automatically install my dotfiles: 
+ `1-package.sh`: Download and Install packages that I use (Only work on Arch, Debian, Fedora, and their derivatives).
+ `2-install.sh`: Copy the config files to your HOME directory as well as giving option to install dwm and/or Openbox.
+ `3-wallpaper.sh`: Download some nice wallpapers.
+ `archsetup.sh`: See **Install on Arch ISO**.

Run `chmod +x *.sh` to make every script executable. 
Please run any installer scripts in their order.

### Install on Arch ISO

**ARTIX is currently unsupported. It will be added in the future.**

Run `archsetup.sh` after connecting Arch installation media to the internet.

	bash <(curl -sL bit.ly/fdarchsetup)

This setup **ONLY WORKS WITH GPT TABLE (UEFI FIRMWARE)**
so make sure that you have at least two partitions for BOOT and ROOT (three if you want separate HOME).
You can run cfdisk before choosing any partition within the setup.

Here are the BTRFS subvolumes that will be created:
| Subvol Number | Subvol Name   | Mountpoint    |
| ---           | ---           | ---           |
| 1             | `@`           | `/`           |
| 2             | `@home`       | `/home`       |
| 3             | `@var_log`    | `/var/log`    | 
| 4             | `@var_cache`  | `/var/cache`  |
| 5             | `@snapshots`  | `/.snapshots` |

`snapper` is used as the backup tool (the setup configures it for you). Check it out [here](https://wiki.archlinux.org/title/Snapper).
`timeshift` can be use as alternative if you don't have snapper installed.

The BOOT partition (ESP) will be mounted to `/boot/efi`.

[easy-arch](https://github.com/classy-giraffe/easy-arch) 
is used as the base for archsetup.sh with manual partition selection (so you can multiboot) 
but without disk encryption and other stuff I don't need.

# Desktop Overview

### Wallpaper
See [WALLPAPER.md](https://gitlab.com/fawzadin/dotfiles/-/blob/master/WALLPAPER.md)
to manually download cool wallpaper that cannot be downloaded through `curl` or `wget`

### dwm
![Screenshot of dwm](https://gitlab.com/fawzadin/dotfiles/raw/master/screenshot/dwm.png)
*wallpaper inspired from [u/atlas-ark](https://reddit.com/u/atlas-ark)*

The `2-install.sh` script can install my build of dwm.
Check it out [here](https://gitlab.com/fawzadin/dwm-fawzadin).

### Awesome
![Screenshot of Awesome](https://gitlab.com/fawzadin/dotfiles/raw/master/screenshot/awesome.png)
*It looks very similar to dwm but trust me. This is Awesome WM.*

The above script can install Awesome WM as dwm alternative.
Useful for distro where compiling dwm is unfeasible (as Awesome is available as normal package in many distros).
Check the config [here](https://gitlab.com/fawzadin/dotfiles/-/tree/master/.config/awesome).

### Openbox
![Screenshot of Openbox](https://gitlab.com/fawzadin/dotfiles/raw/master/screenshot/openbox.png)

The above script can install Openbox as a floating wm alternative. 
Useful for certain programs that very heavily rely on floating window environment.
Check the config [here](https://gitlab.com/fawzadin/dotfiles/-/tree/master/.config/openbox).

### Colorscheme
There's three custom theme/colorscheme you can choose during the installation:
- Matcha-dark-alice
- Matcha-dark-azure
- Nord-darker
See info about the custom themes [here](https://gitlab.com/fawzadin/gtk-theme).
 
if you want to change into the other colorscheme post-install, do the following:
- Change gtk-theme on lxappearance (qt will use the gtk2 version of chosen theme).
- Change colorscheme on Alacritty, Flameshot, and Neovim.
- Recompile suckless-tools (dmenu, st, and tabbed) by uncommenting the color you want to use and comment the current one.
- dwm: Recompile dwm the same way as for suckless-tools
- awesome: Change theme on variable `local chosen_theme` at rc.lua
- openbox: Change theme at rc.xml or use `obconf`. Change tint2rc with the provided colors available in its config directory.

The usage of Xresources and a script to change color will be implemented soon.

# License and Credit
MIT License for all files and scripts unless specified otherwise. Use my dotfiles and script as much as you like.
The lock image is taken from [vecteezy.com](https://vecteezy.com).

