#!/bin/sh
# Do NOT run this as root as most programs cannot be run as root.

# Set some variable.
set -o nounset # error when referencing undefined variable.
set -o errexit # exit when a command fails.

# Setting up our current path.
current="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# Variable for preventing *Main and *Min to be run together
pkgInstall=0

# Make function for each distro's package manager and confirmation query to make typing easier.
pacchk() {
    [ -x "$(command -v pacman)" ]
}

aptchk() {
    [ -x "$(command -v apt)" ]
}

dnfchk() {
    [ -x "$(command -v dnf)" ]
}

confirm() {
    printf "$* (y/N)? "
    read -r REPLY
    [ "$REPLY" = "y" ] || [ "$REPLY" = "Y" ]
}


#=======================#
#||   Main Packages   ||#
#=======================#

# If there's any packages that cannot be installed, it's probably your distro's problem.

#===|| Pacman ||===#
# Main Version
pacMain() {
    [ "$pkgInstall" = 1 ] && echo "> Main and Min cannot be run together." && return
    sudo pacman -Syyu --noconfirm
    sudo pacman -S --noconfirm --needed base-devel alacritty catfish thunar file-roller \
        thunar-archive-plugin thunar-volman gvfs gvfs-mtp \
        picom xorg-xinput redshift opendoas qutebrowser \
        neofetch dunst xcape noto-fonts \
        man ranger mousepad playerctl htop \
        zsh zsh-syntax-highlighting zsh-autosuggestions \
        neovim neovim-qt mpc mpd ncmpcpp \
        mpv flameshot maim nitrogen ristretto \
        zip unzip wget curl \
        lxappearance lxsession lxtask i3lock \
        ttf-ubuntu-font-family xorg-server pacman-contrib

    echo "> Adding current user to enable doas..."
    echo "permit $USER as root" | sudo tee /etc/doas.conf

    echo "> Using all cores for make compile."
    sudo sed -i "s/-j2/-j$(nproc)/;s/^#MAKEFLAGS/MAKEFLAGS/" /etc/makepkg.conf

    # Won't install acpilight if you have xbacklight
    [ -f "/usr/bin/xbacklight" ] && echo "> You may have xbacklight installed." \
        "Please do manual confirmation if you want to replace it with acpilight." && \ 
        sudo pacman -S acpilight
    [ ! -f "/usr/bin/xbacklight" ] && sudo pacman -S --noconfirm acpilight

    confirm "> Do you want to use Pipewire instead of Pulseaudio" && \
        echo "> Confirm manually if you have Pulseaudio and want to replace it." && \
        sudo pacman -S pipewire pipewire-pulse pipewire-alsa pulsemixer pamixer pavucontrol || \
        sudo pacman -S --noconfirm pulseaudio pulseaudio-alsa pulsemixer pamixer pavucontrol 
    
    pkgInstall=1
}

# Minimal Version
pacMin() {
    [ "$pkgInstall" = 1 ] && echo "> Main and Min cannot be run together." && return
    sudo pacman -Syyu --noconfirm
    sudo pacman -S --noconfirm --needed base-devel alacritty catfish redshift neofetch \
        zsh zsh-syntax-highlighting zsh-autosuggestions neovim neovim-qt xcape noto-fonts \
        mpv htop zip unzip wget curl ttf-ubuntu-font-family pacman-contrib opendoas

    echo "> Adding current user to enable doas."
    echo "permit $USER as root" | sudo tee /etc/doas.conf

    pkgInstall=1
}

# AUR. Paru will be used as our main AUR Helper but yay will be used if you have it installed already.
pacAUR() {
    local aurpac="brave-bin xcursor-breeze nwg-launchers spaceship-prompt mpdris2"
    if [ -x "$(command -v paru)" ]; then
        paru -S --noconfirm --needed $aurpac 
    elif [ -x "$(command -v yay)" ]; then
        yay -S --noconfirm --needed $aurpac
    else
        printf "\nEither Paru or Yay is not installed. Do you want to install Paru (y/N)? "
        read -r REPLY
        case "$REPLY" in
            y|Y ) [ -e "$current/paru-bin" ] || git clone https://aur.archlinux.org/paru-bin.git
                cd $current/paru-bin
                makepkg -si --noconfirm
                paru -S --noconfirm $aurpac
                ;;
            * ) echo "> Make sure you install the following AUR packages:"
                echo "> $aurpac" "[Optional]timeshift-bin"
                ;;
        esac
    fi 
    
    # Will ask to install timeshift if you don't have snapper.
    local ts=0
    [ ! -x "$(command -v snapper)" ] && [ ! -x "$(command -v timeshift)" ] && \
        confirm "> No backup tool installed. Do you want to install timeshift?" && ts=1
    if [ "$ts" = "1" ]; then
        [ -x "$(command -v paru)" ] && paru -S --noconfirm timeshift-bin || \
        [ -x "$(command -v yay)" ] && yay -S --noconfirm timeshift-bin || \
        echo "> No AUR helper installed. Please install timeshift manually from AUR." && \
        printf "Press enter to continue... " && \
        read -r REPLY
    fi
}

# Prepare for pacman. Currently, this will only change mirrors to the fastest and most recent mirrors.
pacPrepare() {
    echo "> Changing mirror to the fastest and most recent." 
    [ ! -f "/etc/manjaro-release" ] && [ -x "$(command -v reflector)" ] && \
        sudo reflector -l 10 --protocol https --sort rate --save /etc/pacman.d/mirrorlist && \
        return 
    [ ! -f "/etc/manjaro-release" ] && [ ! -x "$(command -v reflector)" ] && \
        echo "> You don't have reflector. Attempting to install it. This may fail." && \
        sudo pacman -S --noconfirm reflector && \
        sudo reflector -l 10 --protocol https --sort rate --save /etc/pacman.d/mirrorlist && \
        return  
    [ -f "/etc/manjaro-release" ] && \
        sudo pacman-mirrors --fasttrack 10 --api --protocol https && \
        return 
}

#===|| Apt ||===#
# Main Version
aptMain() {
    [ "$pkgInstall" = 1 ] && echo "> Main and Min cannot be run together." && return
    sudo apt update && sudo apt upgrade
    sudo apt install -y build-essential dunst catfish thunar file-roller \
        thunar-archive-plugin thunar-volman gvfs* lxappearance lxtask \
        lxpolkit compton xinput redshift redshift-gtk man \
        neofetch breeze-cursor-theme fonts-noto \
        playerctl qutebrowser timeshift \
        xbacklight zsh zsh-syntax-highlighting zsh-autosuggestions ranger \
        neovim neovim-qt mpd mpc ncmpcpp mpdris2 \
        mpv flameshot maim nitrogen ristretto \
        htop mousepad patch \
        zip unzip gzip wget curl i3lock xserver-xorg

    # Won't install Pulseaudio if you have pipewire on the lastest version of Ubuntu
    [ ! -x "$(command -v pipewire)" ] && sudo apt install -y pulseaudio pulsemixer pavucontrol || \
        sudo apt install -y pulsemixer pavucontrol

    pkgInstall=1
}

# Minimal Version
aptMin() {
    [ "$pkgInstall" = 1 ] && echo "> Main and Min cannot be run together." && return
    sudo apt update && sudo apt upgrade
    sudo apt install -y build-essential catfish redshift redshift-gtk neofetch \
        zsh zsh-syntax-highlighting zsh-autosuggestions fonts-noto \
        neovim neovim-qt mpv htop zip unzip gzip wget curl patch 

    pkgInstall=1
}

# Install Brave Browser
aptBrave() {
    cd $current
    echo "> Installing Brave with the official method."
    sudo apt install -y apt-transport-https curl
    sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
    sudo apt update && sudo apt install -y brave-browser
    [ ! -x "/usr/bin/brave" ] && sudo ln -sf /usr/bin/brave-browser /usr/bin/brave
}

# Remove depedencies for program compilation later in the script.
aptRemove() {
    cd $current
    confirm "> Do you want to remove make/compile depedencies" || return 

    # Special case for Linux Lite as its welcome program hinders our need.
    if [ -e "$HOME/.config/lite" ]; then
        echo "> You are on Linux Lite. We have to remove Lite Welcome in order to remove makedeps."
        echo "> You can install it again with 'sudo apt install lite-welcome'"
        confirm "> Continue" && sudo apt purge --allow-remove-essential -y lite-welcome || return
    fi

    sudo apt purge -y cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev \
        libpulse-dev libboost-program-options-dev libgtkmm-3.0-dev nlohmann-json3-dev 
    sudo apt autoremove -y
}

#===|| Dnf ||===#
# Main Version
dnfMain() {
    [ "$pkgInstall" = 1 ] && echo "> Main and Min cannot be run together." && return
    echo "> Make dnf pick the fastest mirror and enable parralel downloads."
    grep -q "max_parallel_downloads" /etc/dnf/dnf.conf || echo "max_parallel_downloads=5" | sudo tee -a /etc/dnf/dnf.conf
    grep -q "fastestmirror" /etc/dnf/dnf.conf || echo "fastestmirror=True" | sudo tee -a /etc/dnf/dnf.conf
    sudo dnf upgrade -y
    sudo dnf install -y alacritty catfish thunar file-roller \
        thunar-archive-plugin thunar-volman gvfs gvfs-mtp \
        picom xinput redshift opendoas qutebrowser google-noto-fonts-common \
        neofetch dunst man ranger mousepad playerctl htop \
        zsh zsh-syntax-highlighting zsh-autosuggestions \
        neovim neovim-qt mpc mpd ncmpcpp \
        flameshot maim nitrogen ristretto \
        pavucontrol zip unzip gzip wget curl make gcc gcc-c++ gawk \
        lxappearance lxpolkit lxtask i3lock \
        breeze-cursor-theme nwg-launchers xorg-x11-server-Xorg pkgconfig

    echo "> Adding current user to enable doas."
    echo "permit $USER as root" | sudo tee /etc/doas.conf

    # Won't install Pulseaudio if you have pipewire on the lastest version of fedora 
    [ ! -x "$(command -v pipewire)" ] && sudo dnf install -y pulseaudio pavucontrol || \
        sudo dnf install -y pavucontrol

    confirm "> Do you want to add RPM Fusion Free repository to get more apps like mpv" && \
        echo "> Adding RPM Fusion Free repo." && \
        sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

    pkgInstall=1
}

# Minimal Version
dnfMin() {
    [ "$pkgInstall" = 1 ] && echo "> Main and Min cannot be run together." && return
    echo "> Make dnf pick the fastest mirror."
    grep -q "max_parallel_downloads" /etc/dnf/dnf.conf || echo "max_parallel_downloads=5" | sudo tee -a /etc/dnf/dnf.conf
    grep -q "fastestmirror=1" /etc/dnf/dnf.conf || echo "fastestmirror=1" | sudo tee -a /etc/dnf/dnf.conf
    sudo dnf upgrade -y
    sudo dnf install -y alacritty catfish redshift google-noto-fonts-common \
        neofetch htop zsh zsh-syntax-highlighting zsh-autosuggestions \
        neovim neovim-qt mpv zip unzip gzip wget curl patch make gcc gcc-c++ gawk opendoas pkgconfig

    echo "> Adding current user to enable doas."
    echo "permit $USER as root" | sudo tee /etc/doas.conf

    confirm "> Do you want to add RPM Fusion Free repository to get more programs like mpv" && \
        echo "> Adding RPM Fusion Free repo." && \
        sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

    pkgInstall=1
}

# Installing Brave
dnfBrave() {
    cd $current
    echo "> Installing Brave with the official method."
    sudo dnf install -y dnf-plugins-core
    sudo dnf config-manager -y --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
    sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
    sudo dnf install -y brave-browser
    [ ! -x "/usr/bin/brave" ] && sudo ln -sf /usr/bin/brave-browser /usr/bin/brave
}

# Remove depedencies for program compilation later in the script.
dnfRemove() {
    cd $current
    confirm "> Do you want to remove make/compile depedencies" || return 
    sudo dnf remove -y pulseaudio-libs-devel boost-devel
    # Uncomment this line if you want to build Alacritty and remove build depedencies.
    #sudo dnf remove -y cmake freetype-devel fontconfig-devel libxcb-devel
}


#=========================#
#||   Manual Packages   ||#
#=========================#

# Manual packages mean that the programs are compiled and installed from source

# Installing Alacritty Terminal
manAlacritty() {
    pacchk && echo "> $0 will not work on Arch." && return

    # Alacritty doesn't exist in Debian/Ubuntu repo. We have to compile them manually.
    # Alacritty does exist on Fedora official repo but you can still run this to compile it anyway
    # by commenting the first dnfchk and uncomment the second dnfchk.
    # Don't forget to uncomment a line in dnfRemove() too.
    cd $current
    echo "> Preparing to install alacritty."
    aptchk && echo "> Installing Rust and build depedencies." && \
        sudo apt install -y cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev python3
    dnfchk && echo "> Alacritty exists on repo. Installing." && sudo dnf install -y alacritty && return
    #dnfchk && sudo dnf install -y cmake freetype-devel fontconfig-devel libxcb-devel
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rustup.sh && sh $current/rustup.sh -y

    echo "> Installing Alacritty. Compilation may take a while."
    [ -e "$current/alacritty" ] || git clone https://github.com/alacritty/alacritty.git
    cd $current/alacritty
    # Temporary add cargo to path
    export PATH="$HOME/.cargo/bin:$PATH"
    cargo build --release

    echo "> Finishing Alacritty."
    sudo tic -xe alacritty,alacritty-direct extra/alacritty.info
    sudo cp target/release/alacritty /usr/local/bin 
    sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg
    sudo desktop-file-install extra/linux/Alacritty.desktop
    sudo update-desktop-database
    sudo mkdir -p /usr/local/share/man/man1
    gzip -c extra/alacritty.man | sudo tee /usr/local/share/man/man1/alacritty.1.gz > /dev/null
}

# Installing nwg-launchers for logout menu and application grid
# For dwm, awesome, and openbox 
manNWG() {
    pacchk && echo "> $0 will not work on Arch." && return

    cd $current
    echo "> Preparing to install nwg-launchers."
    aptchk && echo "> Installing build depedencies." && \
        sudo apt install -y libgtkmm-3.0-dev nlohmann-json3-dev meson ninja-build
    dnfchk && echo "> nwg-launchers exists on repo. Installing." && sudo dnf install -y nwg-launchers && return

    echo "> Installing nwg-launchers"
    [ -e "$current/nwg-launchers" ] || git clone https://github.com/nwg-piotr/nwg-launchers.git
    cd $current/nwg-launchers
    meson builddir -Dbuildtype=release
    ninja -C builddir # I think i don't need to run this first.
    sudo ninja -C builddir install  
}

# Installing pamixer for easier volume control. Works with pipewire.
manPamixer() {
    pacchk && echo "> $0 will not work on Arch." && return

    cd $current
    echo "> Preparing to install pamixer."
    echo "> Installing build depedencies."
    aptchk && sudo apt install -y libpulse-dev libboost-program-options-dev
    dnfchk && sudo dnf install -y pulseaudio-libs-devel boost-devel

    echo "> Installing pamixer."
    [ -e "$current/pamixer" ] || git clone https://github.com/cdemoulins/pamixer.git
    cd $current/pamixer
    sudo make install
}

# Installing xcape for binding mod keys.
manXcape() {
    pacchk && echo "> $0 will not work on Arch." && return

    cd $current
    echo "> Preparing to install xcape."
    aptchk && echo "> xcape exists on repo. Installing." && sudo apt install -y xcape && return
    dnfchk && echo "> Installing build depedencies." && sudo dnf install -y libX11-devel libXtst-devel libXi-devel

    echo "> Installing xcape."
    [ -e "$current/xcape" ] || git clone https://github.com/alols/xcape.git
    cd $current/xcape
    sudo make install
}

# Installing spaceship for cooler zsh prompt. 
# This function is distro-agnostic.
manSpaceship() {
    cd $current
    echo "> Installing Spaceship prompt for zsh..."
    echo "> Spaceship installation can be found in ~/.config/zsh/prompt"
    mkdir -p $HOME/.config/zsh/prompt
    cd $HOME/.config/zsh/prompt
    [ -e "$HOME/.config/zsh/prompt/spaceship-prompt" ] || git clone https://github.com/denysdovhan/spaceship-prompt.git --depth=1
    cd $HOME/.config/zsh/prompt/spaceship-prompt
    sudo mkdir -p /usr/local/share/zsh/site-functions/
    sudo ln -sf "$HOME/.config/zsh/prompt/spaceship-prompt/spaceship.zsh" "/usr/local/share/zsh/site-functions/prompt_spaceship_setup"
    echo "> Add these lines to your .zshrc to enable spaceship:
    autoload -U promptinit; promptinit
    prompt spaceship"
}


#==================================#
#||   Finisher for All Distros   ||#
#==================================#

# Installing Fira Nerd Fonts
fontFira() {
    local fontdir=/usr/local/share/fonts
    cd $current
    echo "> Downloading Ubuntu Font."
    pacchk && sudo pacman -S --noconfirm ttf-fira-sans ttf-fira-mono ttf-fira-code 
    aptchk && \
        sudo apt install -y fonts-firacode && \
        wget https://www.1001fonts.com/download/fira-sans.zip && \
        wget https://www.1001fonts.com/download/fira-mono.zip && \
        mkdir -p $HOME/.local/share/fonts/FiraSans && \
        unzip -d $HOME/.local/share/fonts/FiraSans fira-sans.zip && \
        unzip -d $HOME/.local/share/fonts fira-mono.zip && \
        mv $HOME/.local/share/fonts/Fira\ Mono $HOME/.local/share/fonts/FiraMono
    dnfchk && sudo dnf install -y mozilla-fira-fonts-common mozilla-fira-sans-fonts mozilla-fira-mono-fonts fira-code-fonts

    echo "> Downloading Fira Nerd Font."
    wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip
    wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraMono.zip
    echo "> Unzipping fonts."
    mkdir -p $HOME/.local/share/fonts/FiraCodeNF
    mkdir -p $HOME/.local/share/fonts/FiraMonoNF
    unzip -d $HOME/.local/share/fonts/FiraCodeNF FiraCode.zip
    unzip -d $HOME/.local/share/fonts/FiraMonoNF FiraMono.zip

}

# Installing Ubuntu font and nerd fonts
fontUbuntu() {
    cd $current
    echo "> Downloading Ubuntu Font."
    echo "> You may want to download it from your distro's repo as well."
    wget -O UbuntuFF.zip https://assets.ubuntu.com/v1/0cef8205-ubuntu-font-family-0.83.zip
    echo "> Unzipping fonts."
    mkdir -p $HOME/.local/share/fonts
    unzip -d $HOME/.local/share/fonts UbuntuFF.zip
    mv $HOME/.local/share/fonts/ubuntu-* $HOME/.local/share/fonts/Ubuntu
    rm -rf $HOME/.local/share/fonts/__MACOSX

    echo "> Downloading Ubuntu Nerd Font."
    wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Ubuntu.zip
    wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/UbuntuMono.zip
    echo "> Unzipping fonts."
    mkdir -p $HOME/.local/share/fonts/UbuntuNF
    mkdir -p $HOME/.local/share/fonts/UbuntuMonoNF
    unzip -d $HOME/.local/share/fonts/UbuntuNF Ubuntu.zip
    unzip -d $HOME/.local/share/fonts/UbuntuMonoNF UbuntuMono.zip
}

# Installing theme
# We will use Matcha GTK Theme and Papirus Icons
themeGTK() {
    cd $current
    [ -f "/etc/manjaro-release" ] && echo "> Manjaro already has Matcha theme and Papirus Icons." && return

    echo "> Preparing to install themes."

    pacchk && sudo pacman -S --noconfirm gtk-engine-murrine gtk-engines

    if [ -x "$(command -v paru)" ]; then
        paru -S --noconfirm xcursor-breeze qt5-styleplugins
    elif [ -x "$(command -v yay)" ]; then
        yay -S --noconfirm xcursor-breeze qt5-styleplugins
    else
        pacchk && echo "> No AUR helper exists. Please install KDE Breeze cursor and qt5-styleplugins manually."
    fi

    aptchk && sudo apt install -y gtk2-engines-murrine gtk2-engines-pixbuf breeze-cursor-theme qt5-style-plugins
    dnfchk && sudo dnf install -y gtk-murrine-engine gtk2-engines breeze-cursor-theme qt5-qtstyleplugins
 
    echo "> Installing Matcha GTK Theme."
    [ -e "$current/Matcha" ] || git clone https://github.com/vinceliuice/Matcha-gtk-theme.git Matcha
    cd $current/Matcha
    bash install.sh

    cd $current
    echo "> Installing Papirus Icons and Folders with the official method."
    wget -qO- https://git.io/papirus-icon-theme-install | DESTDIR="$HOME/.icons" sh
    wget -qO- https://git.io/papirus-folders-install | sh

    echo "> Exporting environment variable to .bash_profile for the use of gtk2 theme for QT5 programs."
    echo "export QT_QPA_PLATFORMTHEME=gtk2" >> $HOME/.bash_profile
    echo "export QT_STYLE_OVERRIDE=gtk2" >> $HOME/.bash_profile
}

displayMGR() {
    # Only install lightdm if you don't have any other (popular) display manager.
    [ -f "/etc/artix-release" ] && echo "> Artix is not supported. Please install a DM manually or use startx." && return
    if [ ! -x "$(command -v lightdm)" ] && [ ! -x "$(command -v gdm3)" ] && [ ! -x "$(command -v sddm)" ]; then 
        pacchk && sudo pacman -S --noconfirm lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
        aptchk && sudo apt install -y lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
        dnfchk && sudo dnf install -y lightdm lightdm-gtk lightdm-gtk-greeter-settings
    else
        echo "> A display manager is already installed." && return
    fi

    #TODO: Enable service for Artix OpenRC
    echo "> Enabling ligthdm."
    sudo systemctl enable lightdm.service
}

finishing() {
    # Enable running executable directly from Thunar and disable middle click for closing tab.
    echo "> Changing thunar settings."
    xfconf-query --channel thunar --property /misc-exec-shell-scripts-by-default --create --type bool --set true
    #xfconf-query --channel thunar --property /misc-tab-close-middle-click --create --type bool --set false

    # Alias nvim to vim.
    echo "> Aliasing nvim to vim."
    echo "alias vim=nvim" >> $HOME/.bashrc

    # Adding cache file
    echo "> Adding cache file for zsh."
    mkdir -p $HOME/.cache/zsh
    touch $HOME/.cache/zsh/zsh_history

    # Fixing redshift not working after a week.
    echo "> Fixing redshift."
    mkdir -p $HOME/.config/systemd/user
    cat > "$HOME/.config/systemd/user/geoclue-agent.service" <<EOF
[Unit]
Description=redshift needs to get a (geo)clue

[Service]
ExecStart=/usr/lib/geoclue-2.0/demos/agent

[Install]
WantedBy=default.target  

EOF
    [ ! -f "/etc/artix-release" ] && systemctl --user enable --now geoclue-agent.service || \
        echo "> Artix is not supported. Please enable the systemd service in ~/.config/systemd/user/geoclue-agent.service" \
        "according to your init system."
    sudo tee -a "/etc/geoclue/geoclue.conf" <<EOF 

[redshift]
allowed=true
system=false
users=

EOF

}


#=======================#
#||   Main Function   ||#
#=======================#

main() {
    # Uncomment any function you don't want to perform but "[PKG]Main" functions.
    if pacchk; then
        echo "> Installing on Arch/Arch based system. Please enter your sudo password to begin installation."
        pacPrepare
        pacMain
        #pacMin
        pacAUR
        fontUbuntu
        themeGTK
        displayMGR
        finishing
    elif aptchk; then
        echo "> Installing on Debian/Ubuntu. Please enter your sudo password to begin installation."
        aptMain
        #aptMin
        aptBrave
        manAlacritty
        manPamixer
        manNWG
        manSpaceship 
        aptRemove
        fontUbuntu
        themeGTK
        displayMGR
        finishing
    elif dnfchk; then
        echo "> Installing on Fedora/Red Hat based system. Please enter your sudo password to begin installation."
        dnfMain
        #dnfMin
        dnfBrave
        manPamixer
        manXcape
        manSpaceship
        dnfRemove
        fontUbuntu
        themeGTK
        displayMGR
        finishing
    else
        echo "> You are not on any supported system. Quitting." && return 1
    fi

    printf "\n> All operations are done! \n"
    printf "> Other things you may want to install:\n"
    printf "> pipe-viewer vscodium \n"
}

main
