# Awesome Window Manager Config
![Screenshot of Awesome](https://gitlab.com/fawzadin/dotfiles/raw/master/screenshot/awesome.png)

Awesome is a window manager forked from dwm. 
This config basically turns it into something similar to my build of dwm visually and behaviorally.

The differences between this and my dwm are the following:
+ Everything can be configured and extended just with Lua.
+ Doesn't need to be recompiled for applying changes in config.
+ Built-in cheatsheet (Activate with mod+F1).
+ Very customizable bar theme.
+ Menu by right-clicking on empty root window or ctrl+Escape.
+ Has its own notification server.
+ Has Tile-left layout (stack area is on the left) but not Deck layout from dwm.
+ Adjustable ammount of column in stack area (with mod+ctrl+h/l).
+ Make a window maximized (where it takes all available screen and cannot be resized but you can still spawn new window on top of it).
+ Slower bootup time (dwm is pure C while awesome is C + Lua).
+ cfacts function is a bit janky compared to dwm.

I only recommend using Awesome over dwm for any of the following reason:
+ You like this way of configuring with lua more than in dwm
+ You want more bar customization
+ Compiling dwm is unfeasible in your current distro (e.g. NixOS)

