-- This config is based on DT's Awesome.columnWM config
-- https://gitlab.com/dwt1/dotfiles/-/tree/master/.config/awesome


--====================--
--|| Load libraries ||-- 
--====================--

local awesome, client, mouse, screen, tag = awesome, client, mouse, screen, tag
local ipairs, string, os, table, tostring, tonumber, type = ipairs, string, os, table, tostring, tonumber, type

-- Standard awesome library
local gears         = require("gears") --Utilities such as color parsing and objects
local awful         = require("awful") --Everything related to window managment
                      require("awful.autofocus")

-- Widget and layout library
local wibox         = require("wibox")

-- Theme handling library
local beautiful     = require("beautiful")

-- Notification library
local naughty       = require("naughty")
naughty.config.defaults['icon_size'] = 100

-- Lain library for additional layout, widgets, to quakes
local lain          = require("lain")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
local hotkeys_popup = require("awful.hotkeys_popup").widget
                      require("awful.hotkeys_popup.keys")
local my_table      = awful.util.table or gears.table -- 4.{0,1} compatibility

-- Handle runtime errors after startup and notify the user.
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end

-- Auto start windowless processes
local function run_once(cmd_arr)
    for _, cmd in ipairs(cmd_arr) do
        awful.spawn.with_shell(string.format("pgrep -u $USER -fx '%s' > /dev/null || (%s)", cmd, cmd))
    end
end

run_once({ "unclutter -root" }) -- entries must be comma-separated

-- Debug function
local function debug_msg(msg)
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Debug Message",
                     text = tostring(msg) })
end

--====================--
--|| Basic Settings ||-- 
--====================--

local confdir = os.getenv("HOME") .. "/.config/awesome"
local themes = {
    "dwm-red",      --1
    "dwm-blue",     --2
    "dwm-nord",     --3
}

-- choose your theme here
local chosen_theme = themes[1]
local theme_path = string.format("%s/themes/%s/theme.lua", confdir , chosen_theme)
beautiful.init(theme_path)

local modkey        = "Mod4"
local cplkey        = "Mod3" -- Capslock is binded as Mod3
local altkey        = "Mod1"
local ctrlkey       = "Control"

-- personal variables
local browser           = "brave"
local browseralt        = "qutebrowser"
local editor            = os.getenv("EDITOR") or "nvim"
local editorgui         = "nvim-qt"
local filemanager       = "thunar"
local mediaplayer       = "mpv"
local scrlocker         = "i3lock -i " .. os.getenv("HOME") .. "/.confg/i3lock/lock.png"
local terminal          = "alacritty"
local termfloat         = terminal .. " --class Alacritty,flterm"
local virtualmachine    = "virt-manager"
local aratio            = { 16, 9 } -- Screen ratio for a function later.

-- awesome variables
local tagnamelist ={
    { "  ", "  ", " 爵 ", "  ", "  ", "  ", "  ", " ﭾ ", "  " }, -- Main tags
    { " 𝟭 ", " 𝟮 ", " 𝟯 ", " 𝗤 ", " 𝗪 ", " 𝗘 ", " 𝗔 ", " 𝗦 ", " 𝗗 " } -- Alt tags
}
local usemain = true
awful.util.tagnames = tagnamelist[1] 
awful.util.terminal = terminal
awful.layout.suit.tile.left.mirror = true
awful.layout.layouts = {     
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    lain.layout.centerwork,
    awful.layout.suit.max,
    awful.layout.suit.floating,
    -- Uncomment to enable layout.
    --awful.layout.suit.tile.bottom,
    --awful.layout.suit.tile.top,
    --awful.layout.suit.fair,
    --awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --awful.layout.suit.spiral.dwindle,
    --awful.layout.suit.max.fullscreen,
    --awful.layout.suit.magnifier,
    --awful.layout.suit.corner.nw,
    --awful.layout.suit.corner.ne,
    --awful.layout.suit.corner.sw,
    --awful.layout.suit.corner.se,
    --lain.layout.cascade,
    --lain.layout.cascade.tile,
    --lain.layout.centerwork.horizontal,
    --lain.layout.termfair,
    --lain.layout.termfair.center,
}

-- lain layouts variable 
lain.layout.termfair.nmaster           = 3
lain.layout.termfair.ncol              = 1
lain.layout.termfair.center.nmaster    = 3
lain.layout.termfair.center.ncol       = 1
lain.layout.cascade.tile.offset_x      = 2
lain.layout.cascade.tile.offset_y      = 32
lain.layout.cascade.tile.extra_padding = 5
lain.layout.cascade.tile.nmaster       = 5
lain.layout.cascade.tile.ncol          = 2

--beautiful.init(string.format(gears.filesystem.get_configuration_dir() .. "/themes/%s/theme.lua", chosen_theme))


--========================--
--|| Quakes/Scratchpads ||-- 
--========================--

-- Try to not reorder quakes as they might not launch when there's no client on a tag.
-- app and names for st
-- app = "st"
-- argname = "-n %s -e [TERMAPP]"
local quakemus = lain.util.quake({ -- Music Player
    app = "alacritty",
    name = "spmus",
    height = 0.88,
    width = 0.80,
    argname = "--class %s -e ncmpcpp",
    vert = "center",
    horiz = "center",
})

local quaketop = lain.util.quake({ -- htop
    app = "alacritty",
    name = "sptop",
    height = 0.88,
    width = 0.80,
    argname = "--class %s -e htop",
    vert = "center",
    horiz = "center",
})

local quake = lain.util.quake({ -- Scratchpad Terminal with tabbed for tabbing
    app = "tabbed",
    name = "spterm",
    height = 0.88,
    width = 0.80,
    --argname = "-n %s -c -r 2 st -w \\'\\' -t Scratchterm",
    argname = "-n %s -c -r 2 alacritty --embed \\'\\' --title Scratchterm",
    vert = "center",
    horiz = "center",
})


--========================--
-- Mouse Binding for tags -- 
--========================--

-- 1: Left, 2: Mid, 3: Right, 4: Scroll Down, 5: Scroll Up 
awful.util.taglist_buttons = my_table.join(
    awful.button({ }, 1, function(t) t:view_only() end),
    awful.button({ modkey }, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end)
)

awful.util.tasklist_buttons = my_table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "tasklist", {raise = true})
    end),
    awful.button({ }, 3, function () -- I can't figure out what this function does.
        local instance = nil

        return function ()
            if instance and instance.wibox.visible then
                instance:hide()
                instance = nil
            else
                instance = awful.menu.clients({theme = {width = 250}})
            end
        end
    end)
)


--============================--
-- Menu, Wallpaper, and Wibox -- 
--============================--
mymainmenu = awful.menu({ items = { 
        { "app menu", "nwggrid", confdir .. "/menu/appmenu.png" },
        { "open terminal", terminal, confdir .. "/menu/terminal.png" },
        { "file manager", filemanager, confdir .. "/menu/fileman.png" },
        { "web browser", browser, confdir .. "/menu/browser.png" },
        { "cheatsheet", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end, confdir .. "/menu/sheet.png" },
        { "edit config", editorgui .. " " .. confdir .. "/rc.lua", confdir .. "/menu/options.png" },
        { "edit theme", editorgui .. " " .. theme_path, confdir .. "/menu/theme.png" },
        { "restart wm", awesome.restart, confdir .. "/menu/loop.png" },
        { "==========" },
        { "leave menu", "nwgbar", confdir .. "/menu/logoutmenu.png" },
        { "lock", scrlocker, confdir .. "/menu/lock.png" },
        { "logout", function() awesome.quit() end, confdir .. "/menu/logout.png" },
        { "suspend", "systemctl suspend", confdir .. "/menu/suspend.png" },
        { "reboot", "systemctl reboot", confdir .. "/menu/reboot.png" },
        { "shutdown", "systemctl poweroff", confdir .. "/menu/shutdown.png"},
    }
})

-- Enable right clicking on empty root window
root.buttons(my_table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end)
))

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", function(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end)

-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(function(s) beautiful.at_screen_connect(s) end)


--==================--
--|| Key Bindings ||-- 
--==================--

--===|| Global Keys ||===---
globalkeys = my_table.join(

    --===|| Window Manager Keybinds ||===--
    awful.key({ modkey }, "F1", hotkeys_popup.show_help,
        {description = "show cheatsheet", group="awesome"}),
    awful.key({ ctrlkey }, "Escape", function () mymainmenu:show() end,
        {description = "show main menu", group = "awesome"}),
    awful.key({ modkey, "Shift" }, "Escape", awesome.restart,
        {description = "reload awesome", group = "awesome"}),
     awful.key({ modkey }, "z", function ()
            for s in screen do
                s.mywibox.visible = not s.mywibox.visible
                if s.mybottomwibox then
                    s.mybottomwibox.visible = not s.mybottomwibox.visible
                end
            end
        end,
        {description = "toggle bar (wibox)", group = "awesome"}),

    --===|| Layout Keybindings ||===--
    awful.key({ modkey }, "l", function () awful.tag.incmwfact( 0.05) end,
        {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey }, "h", function () awful.tag.incmwfact(-0.05) end,
        {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l", function () awful.tag.incnmaster(1, nil, true) end,
        {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h", function () awful.tag.incnmaster(-1, nil, true) end,
        {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "l", function () awful.tag.incncol( 1, nil, true) end,
        {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "h", function () awful.tag.incncol(-1, nil, true) end,
        {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey, "Shift" }, "Tab", function () awful.layout.inc(-1) end,
        {description = "previous layout", group = "layout"}),
    awful.key({ altkey, "Shift" }, "Tab", function () awful.layout.inc(1) end,
        {description = "next layout", group = "layout"}),
    awful.key({ modkey }, "4", function () awful.layout.set(awful.layout.layouts[1]) end,
        {description = "tile layout", group = "layout"}),
    awful.key({ modkey }, "5", function () awful.layout.set(awful.layout.layouts[2]) end,
        {description = "tile left layout", group = "layout"}),
    awful.key({ modkey }, "6", function () awful.layout.set(awful.layout.layouts[3]) end,
        {description = "cwork/cmaster layout", group = "layout"}),
    awful.key({ modkey }, "7", function () awful.layout.set(awful.layout.layouts[4]) end,
        {description = "max/monocle layout", group = "layout"}),
    awful.key({ modkey }, "8", function () awful.layout.set(awful.layout.layouts[5]) end,
        {description = "floating layout", group = "layout"}),
    awful.key({ modkey }, "9", function () awful.layout.inc(-1) end,
        {description = "previous layout", group = "layout"}),
    awful.key({ modkey }, "0", function () awful.layout.inc(1) end,
        {description = "next layout", group = "layout"}),

    -- On the fly useless gaps change
    awful.key({ modkey }, "]", function () lain.util.useless_gaps_resize(1) end,
        {description = "increase gaps", group = "gap"}),
    awful.key({ modkey }, "[", function () lain.util.useless_gaps_resize(-1) end,
        {description = "decrease gaps", group = "gap"}),
    awful.key({ modkey, "Shift" }, "]", function () lain.util.useless_gaps_resize(5) end,
        {description = "increase gaps (bigger)", group = "gap"}),
    awful.key({ modkey, "Shift" }, "[", function () lain.util.useless_gaps_resize(-5) end,
        {description = "decrease gaps (bigger)", group = "gap"}),
    awful.key({ modkey }, "p", function () 
            local scr = awful.screen.focused()
            local tag = scr.selected_tag
            if tag.gap > 0 then
                tag.gap = 0
            elseif tag.gap == 0 then
                tag.gap = beautiful.useless_gap 
            end
            awful.layout.arrange(scr)
        end,
        {description = "toggle gap", group = "gap"}),
    awful.key({ modkey, "Shift" }, "p", function () 
            local scr = awful.screen.focused()
            local tag = scr.selected_tag
            tag.gap = beautiful.useless_gap
            awful.layout.arrange(scr)
        end,
        {description = "restore default gap", group = "gap"}),

    --===|| Tag Keybinds ||===--
    awful.key({ modkey }, "equal", awful.tag.viewnext,
        {description = "view next", group = "tag"}),
    awful.key({ modkey }, "minus", awful.tag.viewprev,
        {description = "view previous", group = "tag"}),
    awful.key({ modkey }, "Tab", awful.tag.history.restore,
        {description = "switch to last tag", group = "tag"}),
    awful.key({ altkey }, "Tab", awful.tag.history.restore,
        {description = "switch to last tag", group = "tag"}),
    awful.key({ modkey, "Shift" }, "z", function()
            if usemain then
                for i = 1, 9 do
                    awful.screen.focused().tags[i].name = tagnamelist[2][i]
                end
                usemain = false
            else
                for i = 1, 9 do
                    awful.screen.focused().tags[i].name = tagnamelist[1][i]
                end
                usemain = true
            end
        end,
        {description = "alternate tags", group = "tag"}),

    --===|| Application Launchers ||===
    awful.key({ modkey }, "Return", function () awful.spawn( terminal ) end,
        {description = "terminal", group = "launch apps"}),
    awful.key({ modkey }, "m", function () awful.util.spawn( filemanager ) end,
        {description = "file manager" , group = "launch apps" }),
    awful.key({ modkey }, "b", function () awful.util.spawn( browser ) end,
        {description = "browser" , group = "launch apps" }),
    awful.key({ modkey, "Shift" }, "b", function () awful.util.spawn( browseralt ) end,
        {description = "browser alt" , group = "launch apps" }),
    awful.key({ modkey }, "v", function () awful.util.spawn( "vscodium" ) end,
        {description = "VSCodium" , group = "launch apps" }),
    awful.key({ modkey, "Shift" }, "v", function () awful.util.spawn( editorgui ) end,
        {description = "text editor" , group = "launch apps" }),
    awful.key({ modkey }, "c", function () awful.util.spawn( "pavucontrol" ) end,
        {description = "Audio Control" , group = "launch apps" }),
    awful.key({ modkey }, "Delete", function () awful.util.spawn( "xkill" ) end,
        {description = "run xkill" , group = "launch apps" }),
    awful.key({ modkey }, "g", function () awful.util.spawn( "nwggrid" ) end,
        {description = "all apps" , group = "launch apps" }),
    awful.key({ modkey }, "Escape", function () awful.util.spawn( "nwgbar" ) end,
        {description = "logout menu" , group = "launch apps" }),
    awful.key({ altkey }, "Escape", function () awful.util.spawn( "nwgbar" ) end,
        {description = "logout menu" , group = "launch apps" }),

    --===|| dmenu scripts ||===--
    awful.key({ modkey }, "/", function () awful.util.spawn( "dmenu_run -b" ) end,
        {description = "run dmenu launcher" , group = "dmenu" }),
    awful.key({ modkey, "Shift" }, "\\", function () awful.util.spawn( "dm-logout" ) end,
        {description = "logout menu" , group = "dmenu" }),
    awful.key({ modkey }, "apostrophe", function () awful.util.spawn( "dm-confedit" ) end,
        {description = "edit config files" , group = "dmenu" }),
    awful.key({ modkey }, "semicolon", function () awful.util.spawn( "dm-man" ) end,
        {description = "man page" , group = "dmenu" }),
    awful.key({ modkey, "Shift" }, "x", function () awful.util.spawn( "dm-kill" ) end,
        {description = "kill running process" , group = "dmenu" }),
    awful.key({ modkey }, "BackSpace", function () awful.util.spawn( "dm-websearch" ) end,
        {description = "search the web" , group = "dmenu" }),
    awful.key({ modkey, "Shift" }, "BackSpace", function () awful.util.spawn( "dm-translate" ) end,
        {description = "translate to clipboard" , group = "dmenu" }),

   --===|| Quake/Scratchpad Keybinds ||===--
    awful.key({ modkey }, "\\", function () quake:toggle() end,
              {description = "dropdown application", group = "super"}),
    awful.key({ modkey }, "n", function () quakemus:toggle() end,
              {description = "dropdown application", group = "super"}),
    awful.key({ modkey }, "x", function () quaketop:toggle() end,
              {description = "dropdown application", group = "super"}),

    --===|| Screen Keybindings ||===--
    awful.key({ modkey }, ".", function () awful.screen.focus_relative(1) end,
        {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey }, ",", function () awful.screen.focus_relative(-1) end,
        {description = "focus the previous screen", group = "screen"}),

    --===|| Multimedia Keys ||===--- 
    -- Screenshot
    awful.key({ }, "Print", function () awful.util.spawn("flameshot full -c") end,
        {description = "flameshot full", group = "screenshots"}),
    awful.key({ ctrlkey }, "Print", function () awful.util.spawn( "flameshot gui" ) end,
        {description = "flameshot selection", group = "screenshots"}),
    -- ALSA volume control
    -- I'm surprised that this works on pulseaudio and that I don't need to use pamixer.
    awful.key({ }, "XF86AudioRaiseVolume",
        function ()
            os.execute(string.format("amixer -q set %s 5%%+", beautiful.volume.channel))
            beautiful.volume.update()
        end),
    awful.key({ }, "XF86AudioLowerVolume",
        function ()
            os.execute(string.format("amixer -q set %s 5%%-", beautiful.volume.channel))
            beautiful.volume.update()
        end),
    awful.key({ }, "XF86AudioMute",
        function ()
            os.execute(string.format("amixer -q set %s toggle", beautiful.volume.togglechannel or beautiful.volume.channel))
            beautiful.volume.update()
        end),
    -- Brightness
    awful.key({ }, "XF86MonBrightnessUp", function () os.execute("xbacklight -inc 5") end),
    awful.key({ }, "XF86MonBrightnessDown", function () os.execute("xbacklight -dec 5") end),
    awful.key({ }, "XF86ScreenSaver", function () os.execute(scrlocker) end),
    -- Player Keys 
    awful.key({ }, "XF86AudioPlay", function () os.execute("playerctl play-pause") end),
    awful.key({ }, "XF86AudioStop", function () os.execute("playerctl stop") end),
    awful.key({ }, "XF86AudioNext", function () os.execute("playerctl next") end),
    awful.key({ }, "XF86AudioPrev", function () os.execute("playerctl previous") end)
)

--===|| Client Keybindings ||===--
clientkeys = my_table.join(
    awful.key({ modkey }, "j", function (c) 
            awful.client.focus.byidx(1) 
            awful.client.next(1, c):raise()        
        end,
        {description = "focus next by index", group = "client"}),
    awful.key({ modkey }, "k", function (c)
            awful.client.focus.byidx(-1) 
            awful.client.next(-1, c):raise()        
        end,
        {description = "focus previous by index", group = "client"}),
    awful.key({ modkey, "Shift" }, "j", function () awful.client.swap.byidx(1) end,
        {description = "swap with next client", group = "client"}),
    awful.key({ modkey, "Shift" }, "k", function () awful.client.swap.byidx(-1) end,
        {description = "swap with previous client", group = "client"}),
    awful.key({ modkey, ctrlkey }, "j", function () awful.client.cycle(true)    end,
        {description = "cycle client clockwise", group = "client"}),
    awful.key({ modkey, ctrlkey }, "k", function () awful.client.cycle(false)    end,
        {description = "cycle client counter-clockwise", group = "client"}),
    awful.key({ modkey }, "r", function () awful.client.focus.byidx(0, awful.client.getmaster()) end,
        {description = "focus to master", group = "client"}),
    awful.key({ modkey, "Shift" }, "r", awful.client.urgent.jumpto,
        {description = "jump to urgent client", group = "client"}),
    -- Changing cwfact in awesome is a bit crappy compared to dwm
    awful.key({ modkey}, "i", function() 
            local height = math.floor(root.size() * aratio[2] / aratio[1])
            local max = height - (150 * (awful.client.idx().num - 1)) -- Measured in pixels 
            if client.focus.height <= max then
                awful.client.incwfact(0.05)
            end
        end,
        {description = "increase client factor", group = "client"}),
    awful.key({ modkey}, "u", function() 
            if client.focus.height >= 100 then -- 100 here is 100 pixelx
                awful.client.incwfact(-0.05) 
            end
        end,
        {description = "decrease client factor", group = "client"}),
    awful.key({ modkey}, "y", function() 
            local totalc = awful.client.idx().num
            if totalc > 1 then 
                awful.client.setwfact(1 / totalc) 
            end
        end,
        {description = "reset client factor", group = "client"}),
   awful.key({ modkey }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey }, "grave", function (c) c:kill() end,
        {description = "close", group = "hotkeys"}),
    awful.key({ modkey }, "space",  awful.client.floating.toggle,
        {description = "toggle floating", group = "client"}),
    awful.key({ modkey }, "o", function (c) c:swap(awful.client.getmaster()) end,
        {description = "move to master", group = "client"}),
    awful.key({ modkey }, "t", function (c) c.sticky = not c.sticky end,
        {description = "toggle sticky client", group = "client"}),
    awful.key({ modkey, "Shift" }, "f",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "maximize client", group = "client"}),
    awful.key({ modkey, "Shift" }, ".", function (c) c:move_to_screen(c.screen.index+1) end,
        {description = "move to next screen", group = "client"}),
    awful.key({ modkey, "Shift" }, ",", function (c) c:move_to_screen(c.screen.index-1) end,
        {description = "move to prev screen", group = "client"})
)

-- Bind all key numbers to tags.
-- In this config, we bind our tag keys to the following table.
local tagkeys = { "1", "2", "3", "q", "w", "e", "a", "s", "d" }

for i = 1, 9 do
    -- Hack to only show tags 1 and 9 in the shortcut window (mod+s)
    local descr_view, descr_toggle, descr_move, descr_toggle_focus
    if i == 1 or i == 9 then
        descr_view = {description = "view tag # (use 123qweasd)", group = "tag"}
        descr_toggle = {description = "toggle tag #", group = "tag"}
        descr_move = {description = "move focused client to tag #", group = "tag"}
        descr_toggle_focus = {description = "toggle focused client on tag #", group = "tag"}
    end
    globalkeys = my_table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, tagkeys[i],
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  descr_view),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, tagkeys[i],
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  descr_toggle),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, tagkeys[i],
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  descr_move),
        awful.key({ modkey, cplkey }, tagkeys[i],
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  descr_move),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, tagkeys[i],
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  descr_toggle_focus)
    )
end

-- Control floating windows with mouse
clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        c.floating = true 
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        c.floating = true 
        awful.mouse.client.resize(c, "bottom_right") 
    end)
)

-- Set keys
root.keys(globalkeys)


--==================--
--|| Window Rules ||-- 
--==================--

-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {

    -- All clients will match this rule.
    { rule = { },
        properties = { 
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen,
            size_hints_honor = false
        }
    },

    -- Don't try to enable the titlebar. It is completely disabled in this config.
    { rule_any = { type = { "dialog", "normal" } },
        properties = { titlebars_enabled = false } },

    -- Put any dialog in the center.
    { rule_any = { type = { "dialog", } },
        properties = { placement = "centered" } },

    -- Put certain program on specific tag.
    -- Tag 3 (Browser)
    { rule_any = { 
        class = {
            "Brave",
            "Firefox",
            "LibreWolf"
            } 
        }, 
        properties = { tag = awful.util.tagnames[3] } },

    -- Tag 4 (Editor)
    { rule = { class = "VSCodium" },
        properties = { tag = awful.util.tagnames[4], maximized = true } },

    -- Tag 6 (Paint)
    { rule_any = { 
        class = {
            "Gimp*", 
            "krita", 
            "Pinta", 
            "inkscape"
        }, 
        role = {
            "gimp-image-window" 
            }
        },
        properties = { tag = awful.util.tagnames[6], maximized = true } },

    -- Maximized clients
    { rule_any = { 
        class = {
            mediaplayer,
            "Vlc",
            "VirtualBox Manager",
            "VirtualBox Machine",
            }
        }, 
        properties = { maximized = true } },

    -- Floating clients
    { rule_any = {
        instance = {
            "DTA",  -- Firefox addon DownThemAll.
            "copyq",  -- Includes session name in class.
        },
        class = { -- I just gonna keep these defaults from DT's config.
            "flterm", -- Floating terminal apps
            "Arandr",
            "Blueberry",
            "Galculator",
            "Gnome-font-viewer",
            "Gpick",
            "Imagewriter",
            "Font-manager",
            "Kruler",
            "MessageWin",  -- kalarm.
            "Oblogout",
            "Pavucontrol",
            "Peek",
            "qutebrowser",
            "Skype",
            "System-config-printer.py",
            "Sxiv",
            "Unetbootin.elf",
            "Wpa_gui",
            "pinentry",
            "veromix",
            "Xfce4-settings-manager",
            "xtightvncviewer"
        },
        name = {
            "Event Tester",  -- xev.
        },
        role = {
            "AlarmWindow",  -- Thunderbird's calendar.
            "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
            "Preferences",
            "setup",
            }
        }, 
        properties = { floating = true, placement = "centered" }},
}


--========================--
--|| Remaining Settings ||-- 
--========================--

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Make every new client opened as slave
    if not awesome.startup then
        awful.client.setslave(c)
    end
    -- Uncomment this and comment the former if you want to spawn client as master
    --[[
    if awesome.startup and
        not c.size_hints.user_position
        and not c.size_hints.program_position then
            awful.placement.no_offscreen(c) -- Prevent clients from being unreachable after screen count changes.
    end
    ]]--
end)

-- No border for maximized clients and if there's only 1 client visible
function border_adjust(c)
    if #awful.screen.focused().clients == 1 or c.maximized then
        c.border_width = 0
    elseif #awful.screen.focused().clients > 1 and not c.maximized then
        c.border_width = beautiful.border_width
        c.border_color = beautiful.border_focus
    end
end

client.connect_signal("focus", border_adjust)
client.connect_signal("property::maximized", border_adjust)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- Autostart
awful.spawn.with_shell("sh -c $HOME/.config/awesome/autostart.sh")

-- Removed behavior
-- * focus on hover
-- * scrolling through root window
-- * titlebar (don't try to enable them in rules)

