# Neovim Basic Config
This is a Neovim configuration with some useful plugins.
It is nothing more than an adequate text editor for writting documents and editing config files
so it has no IDE features like LSP, Git, and whatnot.  

There are no important external depedencies so you can use this config to the fullest outside of Linux like Windows (You can optionally install `ripgrep` for one feature from dashboard).

It also won't be converted to Lua anytime soon.

You need to have separated ginit.vim to configure gui client like nvim-qt or fvim.

If you want those IDE features, please consider using [VSCodium](https://vscodium.com)
or, with Neovim, install either [coc.vim plugin](https://github.com/neoclide/coc.nvim)
or [LunarVim config](https://github.com/ChristianChiarulli/LunarVim).

Despite of previous statement, Personal IDE-like config with coc.vim or built-in LSP is coming soon.

## Plugin list
Any plugins that provide IDE function like git integration are not allowed.
All plugins can be found on github.  

#### Themes 
- `itchyny/lightline.vim`: Better status bar.
- `joshdick/onedark.vim`: A good dark theme.
- `shaunsingh/nord.nvim`: Another good dark theme.
- `junegunn/vim-journal`: Syntax highlighting for plain text
 
#### File Manager
- `lambdalisue/nerdfont.vim`: Nerd font support.
- `lambdalisue/fern.vim`: File Manager for Vim (Faster than NERDTree). 
- `lambdalisue/fern-renderer-nerdfont.vim`: Use Nerd Font for Fern.
- `lambdalisue/fern-bookmark.vim`: Bookmark function for fern.
- `yuki-yano/fern-preview.vim`: Preview file from Fern buffer.
 
#### Visual Aid
- `psliwka/vim-smoothie`: Smooth scrolling with ctrl+d, ctrl+f, ctrl+u and ctrl+b. 
- `vimoxide/vim-quickscope`: Makes using f, F, t, and T function quicker.
- `junegunn/goyo.vim`: Distraction-free writting.
- `junegunn/limelight.vim`: Focus on text by shading unfocused lines.
- `ap/vim-css-color`: CSS color directly on vim.
- `machakann/vim-highlightedyank`: Highlight yanked/copied text.
- `iamcco/markdown-preview.nvim`: Markdown preview through web browser in real time.
 
#### Function 
- `junegunn/fzf`: Fuzzy finder function.
- `junegunn/fzf.vim`: Fuzzy finder for some other plugin.
- `glepnir/dashboard-nvim`: Cool startup page for vim.
- `junegunn/vim-easy-align`: Auto align text.
- `tpope/vim-surround`: Surround words with a character.
- `farmergreg/vim-lastplace`: Save cursor location on quit.
- `preservim/nerdcommenter`: Turn a line into a comment.
- `dkarter/bullets.vim`: Auto bullet point for Markdown.
- `jiangmiao/auto-pairs`: Autopair Paranthesis, Bracket, etc.

#### Future Plugins
- `neoclide/coc.vim`: NVCode-like LSP when I'm too lazy to learn LunarVim (or built-in LSP plugins).
- `vimwiki/vimwiki`: Personal Wiki.
- `preservim/vim-pencil`: Useful plugin for writting (I only need line wrap feature so I made it myself with alt+w).
- `tpope/vim-repeat`: Repeat plugin commands with '.'
- `tpope/vim-abolish`: Abbreviate words.
- `KabbAmine/vCoolor.vim`: Quick color picker.
- `antoinemadec/FixCursorHold.nvim`: Fix hold cursor bug (no plugin uses it at the moment).
- Any inspiration from [this config](https://github.com/Optixal/neovim-init.vim)
 
