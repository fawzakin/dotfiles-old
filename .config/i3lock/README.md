Rename your lock image to "lock.png".
Make sure the image resolution matches your screen resolution.
The provided lock image only comes in 1366x768 and 1920x1080.

