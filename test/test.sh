#!/bin/sh

crt="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
crtsrc=$crt/src
mkdir -p $crtsrc

echo "> Installing nwg-launchers"
[ -e "$crtsrc/nwg-launchers" ] || git clone https://github.com/nwg-piotr/nwg-launchers.git $crtsrc/nwg-launchers

cd $crtsrc/nwg-launchers

echo "hello buddy" > test.txt

