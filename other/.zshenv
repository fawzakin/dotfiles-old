typeset -U PATH path
export PATH="$HOME/.cargo/bin:$HOME/.local/bin:$PATH"
export GTK_IM_MODULE='fcitx'
export QT_IM_MODULE='fcitx'
export SDL_IM_MODULE='fcitx'
export XMODIFIERS='@im=fcitx'

# Other XDG paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

# Doesn't seem to work
#export ANDROID_SDK_HOME="$XDG_CONFIG_HOME"/android
#export ANDROID_AVD_HOME="$XDG_DATA_HOME"/android
#export ANDROID_EMULATOR_HOME="$XDG_DATA_HOME"/android
#export ADB_VENDOR_KEY="$XDG_CONFIG_HOME"/android

# Disable files
export LESSHISTFILE=-

# Fixing Paths
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc
export GEM_PATH="$XDG_DATA_HOME/ruby/gems"
export GEM_SPEC_CACHE="$XDG_DATA_HOME/ruby/specs"
export GEM_HOME="$XDG_DATA_HOME/ruby/gems"
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export GOPATH="$XDG_DATA_HOME"/go
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export ZDOTDIR=$HOME/.config/zsh
export HISTFILE="$XDG_CACHE_HOME"/zsh/zsh_history
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export RIPGREP_CONFIG_PATH="$XDG_CONFIG_HOME/ripgrep/ripgreprc"
export AWT_TOOLKIT=MToolkit
export JAR=/path/to/eclipse.jdt.ls/org.eclipse.jdt.ls.product/target/repository/plugins/org.eclipse.equinox.launcher_1.6.0.v20200915-1508.jar
export GRADLE_HOME=$HOME/.gradle
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/bin/java::")
export JDTLS_CONFIG=$HOME/.local/share/nvim/lspinstall/java/config_linux
export WORKSPACE=$HOME/.workspace

# Scaling
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export QT_SCALE_FACTOR=1
export QT_SCREEN_SCALE_FACTORS="1;1;1"
export GDK_SCALE=1
export GDK_DPI_SCALE=1

# QT Theming
#export QT_QPA_PLATFORMTHEME=qt5ct
export QT_QPA_PLATFORMTHEME=gtk2
export QT_STYLE_OVERRIDE=gtk2

# Fix for OnlyOffice and Java programs. Comment if they cause trouble.
export QT_QPA_PLATFORM=xcb
export _JAVA_AWT_WM_NONREPARENTING=1

# Default Programs
# TERMSWITCH for easy terminal switching
export TERMINAL="alacritty"
export EDITOR="nvim"
export READER="xreader"
export VISUAL="nvim"
export BROWSER="brave"
export VIDEO="mpv"
export IMAGE="ristretto"
export COLORTERM="truecolor"
export OPENER="xdg-open"
export PAGER="less"
export WM="dwm"

# Path
#path=("$HOME/scripts" "$HOME/scripts/alsa" "$HOME/scripts/dragon" "$HOME/scripts/lf" "$HOME/scripts/i3" "$HOME/scripts/pulse"
#	"$HOME/scripts/polybar" "$HOME/scripts/bspwm" "$HOME/scripts/lemonbar" "$HOME/scripts/transmission"
#	"$HOME/bin/tweetdeck-linux-x64" "$XDG_DATA_HOME/ruby/gems/bin" "$HOME/go/bin" "$HOME/.local/share/cargo/bin"
#	"$XDG_DATA_HOME/npm/bin" "$HOME/.local/bin" "$path[@]")
#export PATH

#export FFF_TRASH_CMD="trash-put"
#export FFF_TRASH=~/.local/share/Trash/files
#export FFF_KEY_MKDIR="f"
#export FFF_KEY_MKFILE="i"
#export FFF_TRASH=~/.local/share/Trash/files
#export FFF_FAV1=~/videos/anime
#export FFF_FAV2=~/documents/Uni
#export FFF_FAV3=~/pictures/Wallpapers/wallpapers
#export FFF_MARK_FORMAT="> %f"
#export FFF_FILE_FORMAT=" %f"

#export NNN_BMS='v:~/videos;a:~/videos/anime'
#export NNN_TRASH=1
#export NNN_PLUG='o:fzopen'

# Start blinking
export LESS_TERMCAP_mb=$(tput bold; tput setaf 2) # green
# Start bold
export LESS_TERMCAP_md=$(tput bold; tput setaf 2) # green
# Start stand out
export LESS_TERMCAP_so=$(tput bold; tput setaf 3) # yellow
# End standout
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
# Start underline
export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 1) # red
# End Underline
export LESS_TERMCAP_ue=$(tput sgr0)
# End bold, blinking, standout, underline
export LESS_TERMCAP_me=$(tput sgr0)

# pfetch setting
export PF_INFO="ascii title os host kernel wm shell pkgs memory palette"

