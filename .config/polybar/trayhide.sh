#!/bin/sh
# Credit to treditre on Github

# Get screen size
root_geo=$(xwininfo -root | awk -F'[ +]' '$3 ~ /-geometry/ {print $4}')

while :; do
    win_fills=$(xwininfo -root -tree | grep $root_geo)
    if [ -z "$win_fills" ]; then
        polybar-msg cmd show
    else
        polybar-msg cmd hide
    fi
    sleep 1
done


