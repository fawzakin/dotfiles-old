""" Vim-Pencil setting
let g:pencil#wrapModeDefault = 'soft'   " default is 'hard'
let g:pencil#cursorwrap = 0
let g:pencil#conceallevel = 3 
let g:pencil#textwidth = 100

augroup pencil
  autocmd!
  autocmd FileType markdown,mkd call pencil#init()
  autocmd FileType text         call pencil#init({'wrap': 'hard'})
augroup END

nnoremap <M-w> :PencilToggle<CR>
