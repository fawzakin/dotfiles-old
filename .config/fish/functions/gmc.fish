# Defined in - @ line 1
function gmc --wraps='grub-mkconfig -o /boot/grub/grub.cfg' --wraps='sudo grub-mkconfig -o /boot/grub/grub.cfg' --description 'alias gmc=sudo grub-mkconfig -o /boot/grub/grub.cfg'
  sudo grub-mkconfig -o /boot/grub/grub.cfg $argv;
end
